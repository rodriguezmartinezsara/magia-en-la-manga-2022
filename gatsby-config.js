module.exports = {
  siteMetadata: {
    title: `Magia en la Manga`,
    description: `Encuentra un mago en Galicia con Magia en la Manga. Magia para eventos, empresas, magos para colegios e institutos, todo tipo de celebraciones.`,
    author: `@magiaenlamanga`,
    siteUrl: `https://magiaenlamanga.com/`,
    previewImage: 'https://magiaenlamanga.com/magia-en-la-manga-03.webp',
    menuLinks: {
      es: [
        {
          name: 'Inicio',
          link: '/',
        },
        {
          name: 'Servicios',
          link: '/#servicios',
          subMenu: [
            {
              name: 'Particulares',
              link: '/particulares',
            },
            {
              name: 'Magia educativa',
              link: '/magia-educativa',
            },
            {
              name: 'Eventos públicos',
              link: '/eventos-publicos',
            },
            {
              name: 'Empresas',
              link: '/empresas',
            },
          ],
        },
        {
          name: 'Escuela',
          link: '/escuela-de-magia',
        },
        {
          name: 'Sobre nosotros',
          link: '/sobre-nosotros',
        },
        {
          name: 'Contacto',
          link: '#contacto',
        },
        {
          name: 'Blog',
          link: '/blog',
        },
      ],
      gl: [
        {
          name: 'Inicio',
          link: '/gl',
        },
        {
          name: 'Servizos',
          link: '/gl/#servizos',
          subMenu: [
            {
              name: 'Particulares',
              link: '/gl/particulares',
            },
            {
              name: 'Maxia educativa',
              link: '/gl/maxia-educativa',
            },
            {
              name: 'Eventos públicos',
              link: '/gl/eventos-publicos',
            },
            {
              name: 'Empresas',
              link: '/gl/empresas',
            },
          ],
        },
        {
          name: 'Escola',
          link: '/gl/escola-de-maxia',
        },

        {
          name: 'Sobre nós',
          link: '/gl/sobre-nos',
        },
        {
          name: 'Contacto',
          link: '#contacto',
        },
        {
          name: 'Blog',
          link: '/blog',
        },
      ],
    },
  },
  trailingSlash: 'never',
  plugins: [
    `gatsby-plugin-emotion`,
    `gatsby-transformer-json`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-advanced-sitemap`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://magiaenlamanga.com/',
        sitemap: 'https://magiaenlamanga.com/sitemap.xml',
        // policy: [{ userAgent: '*', allow: '/' }],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
        ignore: [`**/\.*`], // ignore files starting with a dot
      },
    },
    {
      resolve: `gatsby-plugin-gatsby-cloud`,
      options: {
        // as your choice
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Magia en la Manga`,
        start_url: `/`,
        background_color: `#292927`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `standalone`,
        icon: `src/images/icon.svg`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
  ],
  // flags: {
  //   DEV_SSR: true,
  // },
};

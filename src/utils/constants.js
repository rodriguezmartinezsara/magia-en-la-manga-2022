const BREAKPOINTS = {
  small: 576,
  medium: 768,
  large: 1024,
};

export const QUERIES = {
  sm: `(min-width:${BREAKPOINTS.small / 16}rem)`,
  md: `(min-width:${BREAKPOINTS.medium / 16}rem)`,
  lg: `(min-width:${BREAKPOINTS.large / 16}rem)`,
};

export const COLORS = {
  white: '0deg 0% 100%',
  primary: {
    normal: '44deg 78% 73%', //brand yellow
    medium: '50deg 82% 51%',
    dark: '44deg 64% 55%',
  },
  grey: {
    100: '46deg 26% 96%',
    200: '48deg 27% 89%',
    300: '60deg 3% 40%',
    400: '60deg 3% 34%',
    500: '60deg 3% 28%',
    600: '60deg 3% 22%',
    700: '60deg 3% 16%', //brand grey
    800: '60deg 3% 11%',
    900: '60deg 3% 3%',
  },
};

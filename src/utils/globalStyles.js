import { css } from '@emotion/react';

import '@fontsource/lato';
import '@fontsource/lato/700.css';
import '@fontsource/josefin-sans';
import '@fontsource/josefin-sans/500.css';
import '@fontsource/josefin-sans/600.css';
import '@fontsource/josefin-sans/700.css';
import { COLORS } from './constants';

export const globalStyles = css`
  /* Based on CSS reset from https://www.joshwcomeau.com/css/custom-css-reset/ */
  /*
  1. Use a more-intuitive box-sizing model
*/

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }

  /*
    2. Remove default margin
  */

  * {
    margin: 0;
  }

  /*
    3. Allow percentage-based heights in the application
  */

  html,
  body,
  #___gatsby {
    height: 100%;
  }

  /*
    Typographic tweaks!
    4. Add accessible line-height
    5. Improve text rendering
  */

  body {
    //line-height: 1.5;
    -webkit-font-smoothing: antialiased;
    overflow-y: scroll;
  }

  /*
    6. Improve media defaults
  */

  img,
  picture,
  video,
  canvas,
  svg {
    display: block;
    max-width: 100%;
  }

  /*
    7. Remove built-in form typography styles
  */

  input,
  button,
  textarea,
  select {
    font: inherit;
  }

  /*
    8. Avoid text overflows
  */

  p,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  a {
    overflow-wrap: break-word;
  }

  /*
    9. Create a root stacking context
  */

  #root,
  #___gatsby {
    isolation: isolate;
  }

  /* 
  ----- Global styles -----
   */

  #gatsby-focus-wrapper {
    display: flex;
    flex-direction: column;
    min-height: 100%;
  }

  footer {
    margin-top: auto;
  }

  .background-blog {
    background-color: var(--color-grey-100);
  }

  html {
    --color-white: hsl(${COLORS.white});
    --color-primary: hsl(${COLORS.primary['normal']});
    --color-primary-medium: hsl(${COLORS.primary['medium']});
    --color-primary-dark: hsl(${COLORS.primary['dark']});
    --color-grey-100: hsl(${COLORS.grey[100]});
    --color-grey-200: hsl(${COLORS.grey[200]});
    --color-grey-300: hsl(${COLORS.grey[300]});
    --color-grey-400: hsl(${COLORS.grey[400]});
    --color-grey-500: hsl(${COLORS.grey[500]});
    --color-grey-600: hsl(${COLORS.grey[600]});
    --color-grey-700: hsl(${COLORS.grey[700]});
    --color-grey-800: hsl(${COLORS.grey[800]});
    --color-grey-900: hsl(${COLORS.grey[900]});
    --color-menu: hsl(${COLORS.grey[700]} / 0.85);

    --box-shadow: 0 0.9px 1.1px hsl(${COLORS.grey[300]} / 0.12),
      0 3px 3.8px -0.5px hsl(${COLORS.grey[300]} / 0.16),
      0 6.4px 8.1px -1px hsl(${COLORS.grey[300]} / 0.19),
      0 14.3px 18.1px -1.5px hsl(${COLORS.grey[300]} / 0.23);

    --fs-900: clamp(2.8rem, 9.2vw - 1.5rem, 4.7rem);
    --fs-800: clamp(2.6rem, 4.3vw + 1rem, 3.8rem);
    --fs-700: clamp(2.3rem, 8.9vw - 1rem, 4.7rem);
    --fs-600: clamp(2.1rem, 4.7vw + 1rem, 3.1rem);
    --fs-500: clamp(1.8rem, 3vw + 1rem, 3rem);
    --fs-400: clamp(2.1rem, 1.3vw + 1.75rem, 2.5rem);
    --fs-300: clamp(2rem, 4.2vw + 1rem, 3.7rem);
  }

  body {
    font-family: 'Lato', sans-serif;
    font-size: 1.1rem;
  }

  @media (prefers-reduced-motion: no-preference) {
    html {
      scroll-behavior: smooth;
    }
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: 'Josefin Sans', sans-serif;
    color: var(--color-grey-800);
  }

  p,
  li {
    line-height: 1.6;
    color: var(--color-grey-900);
  }
`;

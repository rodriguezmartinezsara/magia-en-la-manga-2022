import { useEffect, useState } from 'react';

export const useCountUp = (end, duration = 3000, startCounter = true) => {
  const [number, setNumber] = useState(0);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (startCounter && number < end) {
        setNumber(number + 1);
      }
    }, duration / end);
    return () => {
      clearTimeout(timer);
    };
  }, [number, end, duration, startCounter]);
  return number;
};

[
  {
    "es": {
      "title": "Magia educativa",
      "slug": "/magia-educativa",
      "metaDescription": "Encuentra un mago para colegios e institutos en Galicia. Espectáculos de magia educativa: igualdad de género, magia en inglés, talleres de magia.",
      "subtitle": "Magia educativa",
      "mainImage": {
        "img": "../images/magia-educativa.jpg",
        "alt": "Logotipo de Magia educativa con Magia en la Manga."
      },
      "mainParagraph": "<p>La magia consigue despertar la curiosidad y la fascinación de los más pequeños, y por eso la magia educativa combina misterio y diversión con educación en contenidos, valores y competencias esenciales.</p><p>Para llevar esta magia a colegios, institutos y bibliotecas de toda Galicia, en <i>Magia en la Manga</i> contamos con una amplia oferta de actividades de magia educativa, con temáticas diferentes y adaptados a las edades de los alumnos. También podemos adaptar el formato a las necesidades de cada centro: actividades presenciales, online, sesiones cortas por aulas… ¡Consúltanos!</p>",
      "showsInfo": {
        "title": "Los espectáculos",
        "subtitle": "Los espectáculos de <i>Magia en la Manga</i> buscan siempre el mayor entretenimiento y sorpresa, pero sin olvidar que sean instructivos; adaptamos los espectáculos a las edades y conocimientos del público para cautivar su interés e introducir contenidos, valores y competencias de forma amena a través de la magia."
      },
      "workshopsInfo": {
        "title": "Los talleres",
        "subtitle": "Muchas niñas y niños sienten curiosidad por la magia y a muchas y muchos les encantaría conocer algunos de los secretos mejor guardados de los mejores ilusionistas. ¡Qué mejor manera de aprender estos increíbles trucos de magia que de la mano de auténticos magos profesionales!"
      },
      "workshops": [
        {
          "smallImage": {
            "img": "../images/the-magic-workshop.jpg",
            "alt": "Cartel del taller de magia educativa en inglés “The Magic Workshop”."
          },
          "title": "The Magic Workshop",
          "synopsis": "<i>“Learning by doing”</i>, o aprender haciendo, es una de las maneras más efectivas y divertidas de aprender una lengua extranjera. Este taller es impartido completamente en inglés y fomenta el uso del inglés en los participantes al mismo tiempo que aprenden divertidos e ingeniosos juegos de magia adaptados a su edad y a su nivel."
        },
        {
          "smallImage": {
            "img": "../images/resolviendo-imposibles.jpg",
            "alt": "Cartel del taller de magia y matemáticas “Resolviendo imposibles”."
          },
          "title": "Resolviendo imposibles",
          "synopsis": "¿Quién iba a decir que las matemáticas podían ser mágicas y sorprendentes? En este taller los asistentes aprenderán ingeniosos trucos de magia basados en sencillos pero interesantes principios matemáticos. Probabilidad, álgebra, combinatoria, geometría… son algunas de las herramientas que emplearemos para demostrar la magia de las matemáticas."
        },
        {
          "smallImage": {
            "img": "../images/magia-en-el-estuche.jpg",
            "alt": "Cartel del taller de magia educativa “Magia en el estuche”."
          },
          "title": "Magia en el estuche",
          "synopsis": "No hay mejor magia que la que se puede hacer con objetos comunes del día a día. En este taller aprenderemos juegos de magia con lápices, ceras de colores, gomas elásticas, papeles… y veremos que las mochilas y estuches están llenos de pequeños milagros con los que sorprender a amigos y familiares."
        }
      ],
      "faq": {
        "title": "Preguntas frecuentes",
        "questions": [
          {
            "id": "1",
            "question": "¿Para que edades están pensadas las actividades?",
            "answer": "<p>Tenemos versiones diferentes de cada espectáculo y taller para grupos de edades diferentes, desde Educación Primaria hasta Bachillerato. También podemos hacer magia para Educación Infantil, ¡pregúntanos para saber en qué consiste!</p><p>Los juegos de magia que explicamos en los talleres son de ejecución sencilla y adaptada a las edades de los participantes sin dejar de ser sorprendentes y divertidos de aprender y realizar.</p>"
          },
          {
            "id": "2",
            "question": "¿En qué espacios se pueden realizar?",
            "answer": "<p>Podemos crear el escenario o espacio para la actividad en prácticamente cualquier espacio del colegio o instituto: sala de usos múltiples, biblioteca, salón de actos… incluso en exterior si la meteorología lo permite. Consúltanos y buscaremos la forma de adaptarnos al espacio disponible.</p>"
          },
          {
            "id": "3",
            "question": "¿Cuánto duran las actividades?",
            "answer": "<p>Cada pase de la actividad tiene una duración de 50 minutos, que se corresponde con una hora lectiva. Por causa de la Covid-19, en el caso de los espectáculos también contamos con un formato de sesiones cortas por aulas de 15 minutos de duración, sin contacto con los alumnos.</p>"
          },
          {
            "id": "4",
            "question": "¿Se pueden hacer varios pases en el mismo día?",
            "answer": "<p>Sí, tanto pases del mismo espectáculo o taller como de actividades diferentes en el mismo día.</p>"
          },
          {
            "id": "5",
            "question": "¿Cuántos alumnos pueden participar?",
            "answer": "<p>En los espectáculos, ¡tantos alumnos como podáis juntar! Eso sí, siempre recomendamos que estén divididos por grupos de edades para ajustar lo mejor posible el contenido del espectáculo. Consúltanos para que podamos buscar la mejor fórmula para llegar a todo el alumnado.</p><p>En caso de los talleres, podemos hacerlos para grupos reducidos en los que la atención esté más individualizada, o para grupos más numerosos si el centro cuenta con medios técnicos para que se vea bien (como pantalla y proyector). ¡Consúltanos sin compromiso para que ningún alumno del colegio o del instituto se quede sin aprender magia!</p>"
          },
          {
            "id": "6",
            "question": "¿Solo se pueden realizar en colegios e institutos?",
            "answer": "<p>No, también podemos hacer estas actividades en bibliotecas, asociaciones, campamentos… ¡Pregúntanos!</p>"
          },
          {
            "id": "7",
            "question": "¿Hasta dónde os desplazáis?",
            "answer": "<p>Podemos desplazarnos a cualquier punto de Pontevedra, A Coruña, Lugo y Ourense, pero también podemos hacer las actividades en formato online para llegar aún más lejos. ¡Consúltanos sin compromiso!</p>"
          }
        ]
      },
      "contact": {
        "title": "¿Quieres saber más?",
        "description": "Si quieres llevar nuestra magia educativa a vuestro centro, ¡no dudes en consultarnos! Buscaremos la mejor manera de adaptarnos a tu centro y a tu alumnado."
      }
    },
    "gl": {
      "title": "Maxia educativa",
      "slug": "/gl/maxia-educativa",
      "metaDescription": "Atopa un mago para colexios e institutos en Galicia. Espectáculos de maxia educativa: igualdade de xénero, maxia en inglés, obradoiros de maxia.",
      "subtitle": "Maxia educativa",
      "mainImage": {
        "img": "../images/magia-educativa.jpg",
        "alt": "Logotipo de Maxia educativa con Magia en la Manga."
      },
      "mainParagraph": "<p>A maxia consegue espertar a curiosidade e a fascinación dos máis pequenos, e por iso a maxia educativa combina misterio e diversión con educación en contidos, valores e competencias esenciais.</p><p>Para levar esta maxia a colexios, institutos e bibliotecas de toda Galicia, en <i>Magia en la Manga</i> contamos cunha ampla oferta de espectáculos e obradoiros de maxia educativa, con temáticas diferentes e con diferentes versións para axeitarse ás idades dos alumnos. Tamén podemos adaptar o formato ás necesidades de cada centro: actividades presenciais, online, sesións curtas por aulas… Consúltanos!</p>",
      "showsInfo": {
        "title": "Os espectáculos",
        "subtitle": "Os espectáculos de <i>Magia en la Manga</i> procuran sempre o maior entretemento e sorpresa, pero sen esquecer que sexan instrutivos; adaptamos os espectáculos ás idades e coñecementos do público para cativar o seu interese e introducir contidos, valores e competencias de xeito ameno a través da maxia."
      },
      "workshopsInfo": {
        "title": "Os obradoiros",
        "subtitle": "Moitas nenas e nenos senten moita curiosidade pola maxia e a moitas e moitos encantaríalles coñecer algúns dos segredos mellor gardados dos mellores ilusionistas. Que mellor maneira de aprender estes incribles trucos de maxia da man de auténticos magos profesionais!"
      },
      "workshops": [
        {
          "smallImage": {
            "img": "../images/the-magic-workshop.jpg",
            "alt": "Cartel do obradoiro de maxia educativa en inglés “The Magic Workshop”."
          },
          "title": "The Magic Workshop",
          "synopsis": "<i>“Learning by doing”</i>, ou aprender facendo, é unha das maneiras máis efectivas e divertidas de aprender unha lingua estranxeira. Este obradoiro de maxia é impartido completamente en inglés e fomenta o uso do inglés nos participantes ao mesmo tempo que aprenden divertidos e enxeñosos xogos de maxia adaptados á súa idade e ao seu nivel."
        },
        {
          "smallImage": {
            "img": "../images/resolvendo-imposibles.jpg",
            "alt": "Cartel do obradoiro de maxia e matemáticas “Resolvendo imposibles”."
          },
          "title": "Resolvendo imposibles",
          "synopsis": "Quen ía dicir que as matemáticas podían ser máxicas e sorprendentes? Neste obradoiro aprenderemos enxeñosos trucos de maxia baseados en sinxelos pero interesantes principios matemáticos. Probabilidade, álxebra, combinatoria, xeometría… son algunhas das ferramentas que empregaremos para demostrar a maxia das matemáticas."
        },
        {
          "smallImage": {
            "img": "../images/maxia-no-estoxo.jpg",
            "alt": "Cartel do obradoiro de maxia educativa “Maxia no estoxo”."
          },
          "title": "Maxia no estoxo",
          "synopsis": "Non hai mellor maxia que a que se pode facer con obxectos comúns do día a día. Neste obradoiro aprenderemos xogos de maxia con lápices, ceras de cores, gomas elásticas, papeis… e veremos que e as mochilas e estoxos están cheos de pequenos milagres cos que sorprender a amigos e familiares."
        }
      ],
      "faq": {
        "title": "Preguntas frecuentes",
        "questions": [
          {
            "id": "1",
            "question": "Para que idades están pensadas as actividades?",
            "answer": "<p>Temos versións diferentes de cada espectáculo e obradoiro para grupos de idades diferentes, dende educación primaria ata bacharelato. Tamén podemos facer maxia para educación infantil, pregúntanos para saber en que consiste!</p><p>Os xogos de maxia que explicamos nos obradoiros son de execución sinxela e adaptada ás idades dos participantes, sen deixar de ser abraiantes e divertidos de aprender e realizar.</p>"
          },
          {
            "id": "2",
            "question": "En que espazos se poden realizar?",
            "answer": "<p>Podemos crear o escenario ou espazo para a actividade en prácticamente calquera espazo do colexio ou instituto: sala de usos múltiples, biblioteca, salón de actos… incluso en exterior se a meteoroloxía o permite. Consúltanos e buscaremos a forma de adaptarnos ao espazo dispoñible.</p>"
          },
          {
            "id": "3",
            "question": "Canto duran as actividades?",
            "answer": "<p>Cada pase da actividade ten unha duración de 50 minutos, que se corresponde cunha hora lectiva. Por mor da Covid-19, no caso dos espectáculos tamén contamos cun formato de sesións curtas por aulas de 15 minutos de duración, sen contacto cos alumnos.</p>"
          },
          {
            "id": "4",
            "question": "Pódense facer varios pases no mesmo día?",
            "answer": "<p>Si, tanto pases do mesmo espectáculo ou obradoiro como de actividades diferentes no mesmo día.</p>"
          },
          {
            "id": "5",
            "question": "Cantos alumnos poden participar?",
            "answer": "<p>Nos espectáculos, tantos alumnos como poidades xuntar! Iso si, sempre recomendamos que estean divididos por grupos de idades para axustar o mellor posible o contido do espectáculo. Consúltanos para que poidamos buscar a mellor fórmula para chegar a todo o alumnado do centro.</p><p>En caso dos obradoiros, podemos facelos para grupos reducidos nos que a atención estea máis individualizada, ou para grupos máis numerosos se o centro conta con medios técnicos para que se vexa ben (como pantalla e proxector). Consúltanos sen compromiso para que ningún alumno do colexio ou do instituto quede sen aprender maxia!</p>"
          },
          {
            "id": "6",
            "question": "Só se poden realizar en colexios e institutos?",
            "answer": "<p>Non, tamén podemos facer estas actividades en bibliotecas, asociacións, campamentos… Pregúntanos!</p>"
          },
          {
            "id": "7",
            "question": "Ata onde vos desprazades?",
            "answer": "<p>Podemos desprazarnos a calquera punto de Pontevedra, A Coruña, Lugo e Ourense, pero tamén podemos facer as actividades en formato online para chegar aínda máis lonxe. Consúltanos sen compromiso!</p>"
          }
        ]
      },
      "contact": {
        "title": "Queres saber máis?",
        "description": "Se queres levar a nosa maxia educativa ao voso centro, non dubides en consultarnos! Procuraremos a mellor maneira de axeitarnos ao teu centro e ao teu alumnado."
      }
    }
  }
]

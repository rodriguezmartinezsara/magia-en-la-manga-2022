import * as React from 'react';
import { Link } from 'gatsby';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { Icon } from './Icon';

export const ServicesIcons = ({ services }) => {
  const servicesIcons = css`
    display: grid;
    grid-template-columns: 1fr;
    margin: 0 auto;
    padding: 0 16px;
    gap: 16px;
    max-width: 450px;
    @media ${QUERIES.sm} {
      grid-template-columns: repeat(2, 1fr);
      max-width: 750px;
      padding: 0 24px;
      gap: 24px;
    }
    @media ${QUERIES.lg} {
      grid-template-columns: repeat(4, 1fr);
      max-width: 1300px;
    }
    & > a {
      text-decoration: none;
      color: inherit;
      min-width: 0px;
      @media (hover: hover) and (prefers-reduced-motion: no-preference) {
        &:hover article,
        &:focus article {
          background-color: var(--color-grey-200);
          transform: scale(1.03);
          box-shadow: var(--box-shadow);
          transition: transform 300ms, background-color 300ms, box-shadow 300ms;
        }
        &:hover svg,
        &:focus svg {
          fill: var(--color-primary-dark);
          transform: scale(1.08);
          transition: fill 300ms, transform 400ms;
        }
      }
    }
    article {
      text-align: center;
      background-color: var(--color-grey-100);
      border-radius: 10px;
      padding: 16px;
      will-change: transform;
      transform-origin: 50% 120%;
      transition: background-color 600ms, transform 700ms, box-shadow 600ms;
      height: 100%;
      @media ${QUERIES.md} {
        padding: 16px 24px;
      }
      h3 {
        text-transform: uppercase;
        font-size: 1.4rem;
        font-weight: 700;
        padding: 8px 0 16px 0;
      }
      svg {
        width: 60%;
        height: auto;
        margin: 0 auto;
        transform-origin: 50% 80%;
        transition: fill 600ms, transform 600ms;
      }
    }
  `;

  return (
    <div css={servicesIcons}>
      {services.map(service => (
        <Link to={service.link} key={service.title}>
          <article>
            <Icon name={service.icon} />
            <h3>{service.title}</h3>
            {service.description && <p>{service.description}</p>}
          </article>
        </Link>
      ))}
    </div>
  );
};

import * as React from 'react';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { css } from '@emotion/react';
import { VisuallyHidden } from './VisuallyHidden';
import { Icon } from './Icon';
import { StaticImage } from 'gatsby-plugin-image';

export const Footer = () => {
  const getData = useStaticQuery(graphql`
    query GetFooterData {
      socialMediaJson {
        socialNetworks {
          icon
          link
        }
      }
    }
  `);

  const socialNetworks = getData.socialMediaJson.socialNetworks;

  const footer = css`
    background-color: var(--color-grey-700);
    text-align: center;
    padding: 24px;
    .image {
      max-width: 450px;
      margin: 0 auto;
      padding: 5px 0 30px;
    }
    p {
      text-transform: uppercase;
      color: var(--color-grey-200);
    }
    .social {
      display: flex;
      justify-content: center;
      a {
        padding: 0 4px;
      }
      svg {
        width: 30px;
        height: auto;
      }
    }
    ul.legal {
      padding: 0;
      li {
        list-style: none;
        padding: 8px 0;
        a {
          color: var(--color-grey-200);
          text-decoration: none;
          @media (hover: hover) and (pointer: fine) {
            &:hover {
              color: var(--color-primary);
            }
          }
        }
      }
    }
  `;

  return (
    <footer css={footer}>
      <div className="image">
        <StaticImage src="../images/aprol.png" alt="Cartel APROL" />
      </div>
      <div className="social">
        {socialNetworks.map(socialNetwork => (
          <a key={socialNetwork.icon} href={socialNetwork.link} target="_blank">
            <Icon name={socialNetwork.icon} />
            <VisuallyHidden>Perfil de {socialNetwork.icon}</VisuallyHidden>
          </a>
        ))}
      </div>
      <ul className="legal">
        <li>
          <Link to="/cookies">Política de cookies</Link>
        </li>
      </ul>
      <p>© {new Date().getFullYear()} Magia en la Manga</p>
    </footer>
  );
};

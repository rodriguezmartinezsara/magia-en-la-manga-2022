import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const MagicianInfo = ({ title, description, video }) => {
  const magicianInfo = css`
    padding: 24px;
    max-width: 450px;
    margin: 0 auto;
    display: flex;
    flex-direction: column-reverse;
    align-items: center;
    gap: 32px;
    @media ${QUERIES.sm} {
      max-width: 800px;
      padding: 24px 32px;
    }
    @media ${QUERIES.md} {
      flex-direction: row;
      gap: 64px;
    }
    @media ${QUERIES.lg} {
      max-width: 1250px;
      padding: 120px 48px 24px;
      gap: 80px;
    }
    h3 {
      font-size: var(--fs-400);
      font-weight: 500;
      position: relative;
      padding-bottom: 16px;
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 0;
        left: 50px;
      }
    }
    p {
      padding-top: 16px;
    }
    .info,
    .video {
      width: 100%;
      flex: 1;
    }
    .video {
      span {
        font-family: 'Josefin Sans', sans-serif;
        font-size: 1.5rem;
        font-weight: 500;
        padding-bottom: 16px;
        display: block;
      }
      .iframe-video-wrapper {
        position: relative;
        width: 100%;
        aspect-ratio: 16/9;
        iframe {
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 100%;
          border: none;
        }
      }
    }
  `;

  return (
    <section css={magicianInfo}>
      <div className="info">
        <h3>{title}</h3>
        {parse(description)}
      </div>
      <div className="video">
        <span>{video.text}</span>
        <div className="iframe-video-wrapper">
          <iframe
            src={video.url}
            title={video.title}
            allowFullScreen
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          />
        </div>
      </div>
    </section>
  );
};

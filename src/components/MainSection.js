import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const MainSection = ({ title, description, image, alt }) => {
  const mainSection = css`
    padding: 24px;
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    gap: 32px;
    max-width: 450px;
    align-items: center;
    @media ${QUERIES.sm} {
      max-width: 800px;
      flex-direction: row-reverse;
      padding: 24px 32px;
    }
    @media ${QUERIES.md} {
      gap: 48px;
    }
    @media ${QUERIES.lg} {
      max-width: 1250px;
      padding: 24px 48px;
    }
    h2 {
      font-weight: 500;
      position: relative;
      padding-bottom: 16px;
      font-size: var(--fs-800);
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 0;
        left: 32px;
      }
    }
    p {
      padding-top: 16px;
    }
    .img {
      img {
        width: 100%;
        height: auto;
        border-radius: 10px;
        box-shadow: var(--box-shadow);
      }
      @media ${QUERIES.sm} {
        flex: 2;
      }
      @media ${QUERIES.lg} {
        margin: -80px 40px 0;
      }
    }
    .info {
      width: 100%;
      @media ${QUERIES.sm} {
        flex: 3;
      }
    }
  `;

  return (
    <section css={mainSection}>
      <div className="img">
        <GatsbyImage
          image={image.childImageSharp.gatsbyImageData}
          alt={alt}
          style={{ overflow: 'inherit' }}
        />
      </div>
      <div className="info">
        <h2>{title}</h2>
        {parse(description)}
      </div>
    </section>
  );
};

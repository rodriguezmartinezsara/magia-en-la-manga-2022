import * as React from 'react';
import { TwitterIcon } from './SVG/TwitterIcon';
import { FacebookIcon } from './SVG/FacebookIcon';
import { InstagramIcon } from './SVG/InstagramIcon';
import { YouTubeIcon } from './SVG/YouTubeIcon';
import { FireworksIcon } from './SVG/FireworksIcon';
import { PuzzleIcon } from './SVG/PuzzleIcon';
import { BoxIcon } from './SVG/BoxIcon';
import { CheersIcon } from './SVG/CheersIcon';
import { RingsIcon } from './SVG/RingsIcon';
import { WandIcon } from './SVG/WandIcon';
import { TheatreIcon } from './SVG/TheatreIcon';
import { EduIcon } from './SVG/EduIcon';
import { CakeIcon } from './SVG/CakeIcon';
import { ConfettiIcon } from './SVG/ConfettiIcon';

export const Icon = ({ name }) => {
  const icons = {
    facebook: <FacebookIcon />,
    twitter: <TwitterIcon />,
    youtube: <YouTubeIcon />,
    instagram: <InstagramIcon />,
    fireworks: <FireworksIcon />,
    puzzle: <PuzzleIcon />,
    box: <BoxIcon />,
    cheers: <CheersIcon />,
    rings: <RingsIcon />,
    wand: <WandIcon />,
    theatre: <TheatreIcon />,
    edu: <EduIcon />,
    cake: <CakeIcon />,
    confetti: <ConfettiIcon />,
  };
  return icons[name];
};

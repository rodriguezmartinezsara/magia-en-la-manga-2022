import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { css } from '@emotion/react';
import { VisuallyHidden } from './VisuallyHidden';
import { Icon } from './Icon';

export const SocialMediaBanner = ({ lang }) => {
  const getData = useStaticQuery(graphql`
    query GetSocialMediaBannerData {
      socialMediaJson {
        es
        gl
        socialNetworks {
          icon
          link
        }
      }
    }
  `);

  const title = getData.socialMediaJson[lang];
  const socialNetworks = getData.socialMediaJson.socialNetworks;

  const socialMediaBanner = css`
    width: 100%;
    text-align: center;
    padding: 24px 20px;
    background-image: url('/fondo-butacas.webp');
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    margin: 40px 0;
    h4 {
      font-size: var(--fs-300);
      font-weight: 400;
      color: var(--color-white);
      padding: 8px 0;
    }
    .social {
      display: flex;
      justify-content: center;
      a {
        padding: 4px;
      }
      svg {
        width: 50px;
        height: auto;
      }
    }
  `;

  return (
    <section css={socialMediaBanner}>
      <h4>{title}</h4>
      <div className="social">
        {socialNetworks.map(socialNetwork => (
          <a key={socialNetwork.icon} href={socialNetwork.link} target="_blank">
            <Icon name={socialNetwork.icon} />
            <VisuallyHidden>Perfil de {socialNetwork.icon}</VisuallyHidden>
          </a>
        ))}
      </div>
    </section>
  );
};

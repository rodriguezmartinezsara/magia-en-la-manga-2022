import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const Contact = ({ title, description, children }) => {
  const contactCss = css`
    max-width: 1350px;
    margin: 0 auto;
    display: flex;
    padding: 40px 24px;
    flex-direction: column-reverse;
    justify-content: center;
    align-items: center;
    gap: 48px;
    @media ${QUERIES.md} {
      padding: 48px 24px;
    }
    @media ${QUERIES.lg} {
      flex-direction: row;
      gap: 64px;
      & > div {
        flex: 1;
      }
    }
    .contact {
      background-color: var(--color-grey-100);
      padding: 16px 24px;
      position: relative;
      max-width: 650px;
      scroll-margin-top: 70px;
      width: 100%;
      overflow-wrap: break-word;
      &::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 0 100%, 100% 100%);
        top: -16px;
        left: 0;
      }
      &::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 100% 0, 100% 100%);
        bottom: -16px;
        left: 0;
      }
      a {
        text-decoration: none;
        color: black;
        font-family: 'Josefin Sans', sans-serif;
        font-weight: 600;
        transition: color 0.4s;
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
      h2 {
        position: relative;
        padding-bottom: 16px;
        font-weight: 500;
        font-size: var(--fs-800);
        &::after {
          content: '';
          background-color: var(--color-primary);
          width: 60px;
          height: 2px;
          position: absolute;
          bottom: 0;
          left: 32px;
        }
      }
      p {
        padding: 16px 0 24px 0;
        position: relative;
        &::after {
          content: '';
          background-color: var(--color-grey-300);
          width: 100%;
          height: 1px;
          position: absolute;
          left: 0;
          bottom: 0;
        }
      }
      ul {
        padding: 24px 0 0 0;

        li {
          list-style: none;

          &:last-of-type {
            padding: 8px 0;
          }
        }
      }
      .tel {
        font-size: 1.5rem;
      }
      .mail {
        font-size: clamp(1.1rem, 9.7vw - 1rem, 1.8rem);
      }
    }
  `;

  return (
    <div css={contactCss}>
      <div id="contacto" className="contact">
        <h2>{title}</h2>
        <p>{description}</p>
        <ul>
          <li>
            <a href="tel:+34659896370" className="tel">
              659 89 63 70
            </a>
          </li>
          <li>
            <a href="tel:+34686300367" className="tel">
              686 30 03 67
            </a>
          </li>
          <li>
            <a href="mailto:info@magiaenlamanga.com" className="mail">
              info@magiaenlamanga.com
            </a>
          </li>
        </ul>
      </div>
      {children}
    </div>
  );
};

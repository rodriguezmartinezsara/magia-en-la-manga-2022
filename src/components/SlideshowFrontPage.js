import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { Link } from 'gatsby';

export const SlideshowFrontPage = ({ lang = 'es' }) => {
  const slideshowFrontPage = css`
    width: 100%;
    height: 300px;
    position: relative;
    overflow: hidden;
    isolation: isolate;
    display: flex;
    @media ${QUERIES.md} {
      height: 100vh;
    }
    .slideshow-image {
      position: absolute;
      width: 100%;
      height: 100%;
      background: no-repeat 50% 50%;
      background-size: cover;
      animation-timing-function: linear;
      animation-iteration-count: infinite;
      animation-duration: 8s;
      transform: scale(1.2);
    }
    .slideshow-image:nth-of-type(1) {
      background-image: url('/magia-en-la-manga-02.webp');
      animation-name: zoom-1;
      z-index: 1;
    }
    .slideshow-image:nth-of-type(2) {
      background-image: url('/magia-en-la-manga-03.webp');
      animation-name: zoom-2;
      z-index: 0;
    }
    .info {
      align-self: end;
      z-index: 50;
      padding: 20px 24px;
      width: 100%;
      max-width: 450px;
      margin: 0 auto;
      display: flex;
      gap: 16px;
      flex-flow: column wrap;
      @media ${QUERIES.sm} {
        padding: 24px 32px;
        max-width: 800px;
      }
      @media ${QUERIES.md} {
        padding: 48px 32px;
        gap: 32px;
      }
      @media ${QUERIES.lg} {
        padding: 72px 48px;
        max-width: 1650px;
      }
      h1 {
        color: var(--color-white);
        font-size: var(--fs-700);
        font-weight: 600;
      }
      .button {
        padding: 8px 16px;
        font-weight: 700;
        border-radius: 5px;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        transition: background-color 0.4s, color 0.4s;
        color: black;
        background-color: var(--color-primary);
        margin-left: 5px;
        @media ${QUERIES.md} {
          padding: 16px 24px;
        }
        &:hover {
          background-color: var(--color-primary-dark);
        }
      }
    }
    @keyframes zoom-1 {
      0% {
        opacity: 1;
        transform: scale(1);
      }
      46% {
        opacity: 1;
      }
      54% {
        opacity: 0;
        transform: scale(1.1);
      }
      95% {
        transform: scale(1);
      }
      96% {
        opacity: 0;
      }
      100% {
        opacity: 1;
        transform: scale(1);
      }
    }
    @keyframes zoom-2 {
      0% {
        opacity: 1;
        transform: scale(1);
      }
      46% {
        opacity: 1;
        transform: scale(1);
      }
      96% {
        opacity: 1;
      }
      100% {
        opacity: 0;
        transform: scale(1.1);
      }
    }
  `;

  return (
    <section css={slideshowFrontPage}>
      <div className="slideshow-image"></div>
      <div className="slideshow-image"></div>
      <div className="info">
        <h1>Magos en Galicia</h1>
        <div>
          <Link
            to={lang === 'es' ? '#servicios' : '#servizos'}
            className="button"
          >
            {lang === 'es' ? 'Servicios' : 'Servizos'}
          </Link>
        </div>
      </div>
    </section>
  );
};

import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';
import { VisuallyHidden } from './VisuallyHidden';
import { Icon } from './Icon';

export const TeamMembers = ({ teamMembers }) => {
  const team = css`
    margin: 0 auto;
    max-width: 450px;
    padding: 0 24px 32px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 32px;
    @media ${QUERIES.sm} {
      flex-flow: row wrap;
      max-width: 700px;
      gap: 40px 32px;
      padding: 0 32px 24px;
    }
    @media ${QUERIES.lg} {
      max-width: 1300px;
    }
    & > div {
      @media ${QUERIES.sm} {
        flex: 1 1 min(40vw, 300px);
      }
      @media ${QUERIES.md} {
        flex: 1;
      }
    }
    .img-and-social {
      position: relative;
    }
    img {
      border-radius: 10px;
      width: 100%;
      height: auto;
    }
    h3 {
      font-size: 1.9rem;
      font-weight: 600;
      padding: 24px 0 16px 0;
    }
    .social {
      display: flex;
      justify-content: center;
      position: absolute;
      bottom: 18px;
      left: 50%;
      transform: translate(-50%, 0);
      svg {
        padding: 4px;
        width: 50px;
        height: auto;
      }
    }
  `;

  return (
    <div css={team}>
      {teamMembers.map(teamMember => (
        <div key={teamMember.title}>
          <div className="img-and-social">
            <GatsbyImage
              image={teamMember.image.img.childImageSharp.gatsbyImageData}
              alt={teamMember.image.alt}
            />
            <SocialNetworks
              socialNetworks={teamMember.socialNetworks}
              teamMemberName={teamMember.title}
            />
          </div>
          <h3>{teamMember.title}</h3>
          <p>{parse(teamMember.description)}</p>
        </div>
      ))}
    </div>
  );
};

const SocialNetworks = ({ socialNetworks, teamMemberName }) => (
  <div className="social">
    {socialNetworks.map(socialNetwork => (
      <a key={socialNetwork.icon} href={socialNetwork.link} target="_blank">
        <Icon name={socialNetwork.icon} />
        <VisuallyHidden>
          Perfil de {socialNetwork.icon} de {teamMemberName}
        </VisuallyHidden>
      </a>
    ))}
  </div>
);

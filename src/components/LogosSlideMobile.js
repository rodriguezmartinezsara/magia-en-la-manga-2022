import React, { useEffect, useState } from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import { GatsbyImage } from 'gatsby-plugin-image';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const LogosSlideMobile = ({ logos }) => {
  //cosa que me mandó poner el capullo de Carlos y que no entiendo
  //para solucionar un error horrible que dice así: "Uncaught Error: Minified React error"
  //https://stackoverflow.com/a/72318597/2365348
  const [domLoaded, setDomLoaded] = useState(false);
  useEffect(() => {
    setDomLoaded(true);
  }, []);

  const logoSlideMobile = css`
    padding: 0 24px 32px;
    max-width: 750px;
    margin: 0 auto;
    position: relative;
    &::before {
      content: '';
      background-color: var(--color-grey-200);
      width: 75%;
      height: 1px;
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
      top: -16px;
    }
    &::after {
      content: '';
      background-color: var(--color-grey-200);
      width: 75%;
      height: 1px;
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
      bottom: 16px;
    }
    @media ${QUERIES.md} {
      padding: 0 24px 48px;
      &::after {
        bottom: 32px;
      }
    }
    @media ${QUERIES.lg} {
      display: none;
    }
    img {
      width: 100%;
      height: auto;
      padding: 8px;
      @media ${QUERIES.sm} {
        padding: 8px 16px;
      }
    }
  `;

  const responsiveSettings = [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      },
    },
  ];

  return (
    <>
      {domLoaded && (
        <Slide
          css={logoSlideMobile}
          slidesToScroll={2}
          slidesToShow={2}
          responsive={responsiveSettings}
          duration={3000}
          arrows={false}
          easing={'ease'}
        >
          {logos.map(logo => (
            <GatsbyImage
              key={logo.alt}
              alt={logo.alt}
              image={logo.img.childImageSharp.gatsbyImageData}
            />
          ))}
        </Slide>
      )}
    </>
  );
};

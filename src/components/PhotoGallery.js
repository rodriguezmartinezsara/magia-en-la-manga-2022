import * as React from 'react';
import { Gallery, Item } from 'react-photoswipe-gallery';
import 'photoswipe/dist/photoswipe.css';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const PhotoGallery = ({ images }) => {
  const photoGallery = css`
    display: none;
    @media ${QUERIES.md} {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      gap: 16px;
      padding: 0 16px 54px;
      max-width: 2000px;
      margin: 0 auto;
    }
    img {
      object-fit: cover;
      width: 100%;
      height: auto;
      aspect-ratio: 3/2;
      cursor: pointer;
    }
  `;

  return (
    <Gallery>
      <div css={photoGallery}>
        {images.map(image => (
          <Item
            original={
              image.img.childImageSharp.gatsbyImageData.images.fallback.src
            }
            thumbnail={
              image.img.childImageSharp.gatsbyImageData.images.fallback.src
            }
            cropped
            width={image.width}
            height={image.height}
            key={image.id}
          >
            {({ ref, open }) => (
              <div onClick={open} ref={ref}>
                <GatsbyImage
                  alt={image.alt}
                  image={image.img.childImageSharp.gatsbyImageData}
                />
              </div>
            )}
          </Item>
        ))}
      </div>
    </Gallery>
  );
};

import * as React from 'react';
import { Link } from 'gatsby';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const MainSectionFrontPage = ({
  title,
  description,
  link,
  linkText,
  image,
  alt,
}) => {
  const mainSectionFrontPage = css`
    padding: 24px;
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    gap: 32px;
    max-width: 450px;
    isolation: isolate;
    @media ${QUERIES.sm} {
      max-width: 800px;
      flex-direction: row;
      align-items: center;
      padding: 24px 32px;
    }
    @media ${QUERIES.md} {
      gap: 48px;
    }
    @media ${QUERIES.lg} {
      max-width: 1250px;
      padding: 24px 48px;
    }
    h1 {
      position: relative;
      padding-bottom: 16px;
      font-size: var(--fs-900);
      font-weight: 500;
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 0;
        left: 32px;
      }
    }
    p {
      padding: 16px 0;
    }
    a {
      text-decoration: none;
      color: var(--color-grey-300);
      display: block;
      transition: color 0.4s;
      @media (hover: hover) and (pointer: fine) {
        &:hover {
          color: var(--color-primary-dark);
        }
      }
    }
    .img {
      img {
        width: 100%;
        height: auto;
        border-radius: 10px;
        box-shadow: var(--box-shadow);
      }
      @media ${QUERIES.sm} {
        flex: 2;
      }
      @media ${QUERIES.lg} {
        margin: -80px 40px 0;
      }
    }
    .info {
      @media ${QUERIES.sm} {
        flex: 3;
      }
    }
  `;

  return (
    <section css={mainSectionFrontPage}>
      <div className="info">
        <h1>{title}</h1>
        {parse(description)}
        <Link to={link}>{linkText}</Link>
      </div>
      <div className="img">
        <GatsbyImage
          image={image.childImageSharp.gatsbyImageData}
          alt={alt}
          style={{ overflow: 'inherit' }}
        />
      </div>
    </section>
  );
};

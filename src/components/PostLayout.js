import * as React from 'react';
import { css } from '@emotion/react';
import { Link } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import parse from 'html-react-parser';
import { QUERIES } from '../utils/constants';

export const PostLayout = ({ img, previousPost, nextPost, content }) => {
  const postLayout = css`
    .content {
      background-color: white;
      border-radius: 0 0 10px 10px;
      padding: 24px;
      box-shadow: var(--box-shadow);
      @media ${QUERIES.md} {
        padding: 24px 40px;
      }
      p {
        padding: 8px 0;
      }
      .image {
        img {
          padding: 18px 0;
        }
      }
      a {
        text-decoration: none;
        color: var(--color-grey-300);
        transition: color 0.4s;
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
      h2 {
        font-size: 1.5rem;
        padding: 24px 0;
      }
    }
    .mainImage {
      object-fit: cover;
      width: 100%;
      aspect-ratio: 5 / 2;
    }
  `;

  return (
    <article css={postLayout}>
      <GatsbyImage
        className="mainImage"
        image={img.img.childImageSharp.gatsbyImageData}
        alt={img.alt}
      />
      <section className="content">
        {content.map(
          item =>
            (item.paragraph && <p key={item.id}>{parse(item.paragraph)}</p>) ||
            (item.subtitle && <h2 key={item.id}>{item.subtitle}</h2>) ||
            (item.image && (
              <GatsbyImage
                key={item.id}
                className="image"
                image={item.image.img.childImageSharp.gatsbyImageData}
                alt={item.image.alt}
                style={{ overflow: 'inherit' }}
              />
            )) ||
            (item.columns && <Columns key={item.id} columns={item.columns} />)
        )}
      </section>
      <NavLinks previousPost={previousPost} nextPost={nextPost} />
    </article>
  );
};

export const Columns = ({ columns }) => {
  const columnsCSS = css`
    img {
      padding: 18px 0;
    }
    @media ${QUERIES.md} {
      display: flex;
      gap: 32px;
      align-items: center;
      & > div {
        flex: 1;
      }
      img {
        padding: 16px;
      }
    }
  `;
  return (
    <div css={columnsCSS}>
      <div className="paragraphs">
        {columns.paragraphs.map(paragraph => (
          <p key={paragraph}>{parse(paragraph)}</p>
        ))}
      </div>
      <GatsbyImage
        image={columns.image.img.childImageSharp.gatsbyImageData}
        alt={columns.image.alt}
        style={{ overflow: 'inherit' }}
      />
    </div>
  );
};

export const NavLinks = ({ previousPost, nextPost }) => {
  const navLinks = css`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    justify-content: space-around;
    padding-top: 32px;
    position: relative;
    &::after {
      content: '';
      position: absolute;
      height: 35%;
      width: 1px;
      left: 50%;
      top: 50%;
      background-color: var(--color-grey-300);
    }
    a {
      text-decoration: none;
      color: var(--color-grey-300);
      transition: color 0.4s;
      @media (hover: hover) and (pointer: fine) {
        &:hover {
          color: var(--color-primary-dark);
        }
      }
    }
    p {
      color: inherit;
    }
    p:first-of-type {
      text-transform: uppercase;
      font-size: 0.9rem;
      font-weight: bold;
      padding: 0 20px 12px;
      position: relative;
    }
    .previous.link {
      grid-column: 1/2;
      p:first-of-type::before {
        content: '←';
        position: absolute;
        left: 0;
      }
    }
    .next.link {
      grid-column: 2/3;
      text-align: end;
      p:first-of-type::after {
        content: '→';
        position: absolute;
        right: 0;
      }
    }
  `;

  return (
    <div css={navLinks}>
      {previousPost && (
        <div className="previous link">
          <Link to={previousPost.slug}>
            <p>Anterior</p>
            <p>{previousPost.title}</p>
          </Link>
        </div>
      )}
      {nextPost && (
        <div className="next link">
          <Link to={nextPost.slug}>
            <p>Siguiente</p>
            <p>{nextPost.title}</p>
          </Link>
        </div>
      )}
    </div>
  );
};

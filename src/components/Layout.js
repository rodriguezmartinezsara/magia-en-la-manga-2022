/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import PropTypes from 'prop-types';

import Header from './Header';
import { Footer } from './Footer';
import { Global } from '@emotion/react';
import { globalStyles } from '../utils/globalStyles';
import { getCookieConsentValue } from 'react-cookie-consent';
import { Script } from 'gatsby';
import React, { useState } from 'react';
import { CookiesBanner } from './CookiesBanner';

CookiesBanner.propTypes = { onAccept: PropTypes.func };
const Layout = ({ children, lang, translationSlug }) => {
  const [gpdrAccepted, setGpdrAccepted] = useState(
    getCookieConsentValue('accepted-gdpr') === 'true'
  );

  return (
    <>
      <Global styles={globalStyles} />
      <Header lang={lang} translationSlug={translationSlug} />
      <main>{children}</main>
      <Footer />
      <CookiesBanner setGpdrAccepted={setGpdrAccepted} />
      {gpdrAccepted && (
        <>
          <Script src="https://www.googletagmanager.com/gtag/js?id=UA-208537539-1"></Script>
          <Script
            id="first-unique-id"
            dangerouslySetInnerHTML={{
              __html: `  window.dataLayer = window.dataLayer || []; function gtag()
            {dataLayer.push(arguments)}
            gtag('js', new Date()); gtag('config', 'UA-208537539-1');`,
            }}
          />
        </>
      )}
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;

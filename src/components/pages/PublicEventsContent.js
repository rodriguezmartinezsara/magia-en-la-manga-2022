import * as React from 'react';
import { TopImage } from '../TopImage';
import { SectionTitle } from '../SectionTitle';
import { ImageTitleDescription } from '../ImageTitleDescription';
import { Contact } from '../Contact';

export const PublicEventsContent = ({
  publicEvents,
  shows,
  workshops,
  lang,
}) => {
  return (
    <>
      <TopImage title={publicEvents.title} />
      <section>
        <SectionTitle
          title={publicEvents.showsInfo.title}
          subtitle={publicEvents.showsInfo.subtitle}
        />
        <ImageTitleDescription shows={shows} lang={lang} />
      </section>
      <section>
        <SectionTitle
          title={publicEvents.workshopsInfo.title}
          subtitle={publicEvents.workshopsInfo.subtitle}
        />
        <ImageTitleDescription shows={workshops} lang={lang} />
      </section>
      <Contact
        title={publicEvents.contact.title}
        description={publicEvents.contact.description}
      />
    </>
  );
};

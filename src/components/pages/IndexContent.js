import * as React from 'react';
import { SlideshowFrontPage } from '../SlideshowFrontPage';
import { MainSectionFrontPage } from '../MainSectionFrontPage';
import { SectionTitle } from '../SectionTitle';
import { ServicesIcons } from '../ServicesIcons';
import { SocialMediaBanner } from '../SocialMediaBanner';
import { Contact } from '../Contact';
import { Counter } from '../Counter';
import { PhotoGallery } from '../PhotoGallery';
import { LogosGalleryDesktop } from '../LogosGalleryDesktop';
import { PhotosSlideMobile } from '../PhotosSlideMobile';
import { LogosSlideMobile } from '../LogosSlideMobile';
import { css } from '@emotion/react';

export const IndexContent = ({ infoIndex, lang }) => {
  const services = css`
    scroll-margin-top: 60px;
  `;

  return (
    <>
      <SlideshowFrontPage lang={lang} />
      <MainSectionFrontPage
        title={infoIndex.mainSection.title}
        description={infoIndex.mainSection.description}
        link={infoIndex.mainSection.link}
        linkText={infoIndex.mainSection.linkText}
        image={infoIndex.mainSection.image.img}
        alt={infoIndex.mainSection.image.alt}
      />
      <section id={lang === 'es' ? 'servicios' : 'servizos'} css={services}>
        <SectionTitle title={infoIndex.services.title} />
        <ServicesIcons services={infoIndex.services.typeOfService} />
      </section>
      <SocialMediaBanner lang={lang} />
      <section id="gallery">
        <SectionTitle title={infoIndex.gallery.title} />
        <PhotoGallery images={infoIndex.gallery.images} />
        <PhotosSlideMobile photos={infoIndex.gallery.images} />
      </section>
      <section id="clients">
        <SectionTitle title={infoIndex.clients.title} />
        <LogosGalleryDesktop logos={infoIndex.clients.logos} />
        <LogosSlideMobile logos={infoIndex.clients.logos} />
      </section>
      <Contact
        title={infoIndex.contact.title}
        description={infoIndex.contact.description}
      />
      <Counter lang={lang} />
    </>
  );
};

import * as React from 'react';
import { TopImage } from '../TopImage';
import { MainSection } from '../MainSection';
import { SectionTitle } from '../SectionTitle';
import { ImageTitleDescription } from '../ImageTitleDescription';
import { ImageTitleDescriptionWorkshops } from '../ImageTitleDescriptionWorkshops';
import { Faq } from '../Faq';
import { Contact } from '../Contact';

export const EducationalEventsContent = ({
  educationalEvents,
  shows,
  lang,
}) => {
  return (
    <>
      <TopImage title={educationalEvents.title} />
      <MainSection
        title={educationalEvents.subtitle}
        description={educationalEvents.mainParagraph}
        image={educationalEvents.mainImage.img}
        alt={educationalEvents.mainImage.alt}
      />
      <section>
        <SectionTitle
          title={educationalEvents.showsInfo.title}
          subtitle={educationalEvents.showsInfo.subtitle}
        />
        <ImageTitleDescription shows={shows} lang={lang} />
      </section>
      <section>
        <SectionTitle
          title={educationalEvents.workshopsInfo.title}
          subtitle={educationalEvents.workshopsInfo.subtitle}
        />
        <ImageTitleDescriptionWorkshops
          workshops={educationalEvents.workshops}
        />
      </section>
      <section>
        <SectionTitle title={educationalEvents.faq.title} />
        <Faq questions={educationalEvents.faq.questions} />
      </section>
      <Contact
        title={educationalEvents.contact.title}
        description={educationalEvents.contact.description}
      />
    </>
  );
};

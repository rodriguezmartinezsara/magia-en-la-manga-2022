import * as React from 'react';
import { TopImage } from '../TopImage';
import { MainSection } from '../MainSection';
import { ImageParagraph } from '../ImageParagraph';
import { MagicianInfo } from '../MagicianInfo';
import { CharacteristicsGoals } from '../CharacteristicsGoals';
import { Contact } from '../Contact';
import { GatsbyImage } from 'gatsby-plugin-image';
import { css } from '@emotion/react';
import { EnglishActivities } from '../EnglishActivities';

export const ShowContent = ({ show, contact }) => {
  return (
    <>
      <TopImage title={show.title} />
      <MainSection
        title={show.subtitle}
        description={show.mainParagraph}
        image={show.mainImage.img}
        alt={show.mainImage.alt}
      />
      <ImageParagraph
        image={show.secondImage.img}
        text={show.secondParagraph}
        alt={show.secondImage.alt}
      />
      {show.magicianInfo && (
        <MagicianInfo
          title={show.magicianInfo.title}
          description={show.magicianInfo.description}
          video={show.magicianInfo.video}
        />
      )}
      {show.activities && <EnglishActivities activities={show.activities} />}
      <CharacteristicsGoals
        characteristics={show.characteristics}
        goalsTitle={show.goals?.title}
        goalsItems={show.goals?.items}
      />
      <Contact title={contact.title} description={contact.description}>
        {show.emega && <EmegaImg img={show.emega.img} alt={show.emega.alt} />}
      </Contact>
    </>
  );
};

export const EmegaImg = ({ img, alt }) => {
  const emegaImg = css`
    max-width: 500px;
    img {
      width: 100%;
      height: auto;
    }
  `;
  return (
    <div css={emegaImg}>
      <GatsbyImage
        image={img.childImageSharp.gatsbyImageData}
        alt={alt}
        style={{ overflow: 'inherit' }}
      />
    </div>
  );
};

import * as React from 'react';
import { TopImage } from '../TopImage';
import { SectionTitle } from '../SectionTitle';
import { ServicesIcons } from '../ServicesIcons';
import { SectionItem } from '../SectionItem';
import { Contact } from '../Contact';
import { WidgetBodasNet } from '../WidgetBodasNet';

export const PrivateEventsContent = ({ privateEvents }) => {
  return (
    <>
      <TopImage title={privateEvents.title} />
      <section>
        <SectionTitle
          title={privateEvents.subtitle}
          subtitle={privateEvents.description}
        />
        <ServicesIcons services={privateEvents.servicesHeader} />
      </section>
      {privateEvents.typeOfEvent.map(privateEvent => (
        <SectionItem
          key={privateEvent.title}
          title={privateEvent.title}
          description={privateEvent.description}
          characteristics={privateEvent.characteristics}
          link={privateEvent.link}
          image={privateEvent.image.img}
          alt={privateEvent.image.alt}
        />
      ))}

      <Contact
        title={privateEvents.contact.title}
        description={privateEvents.contact.description}
      >
        <WidgetBodasNet reviewsQuantity={5} />
      </Contact>
    </>
  );
};

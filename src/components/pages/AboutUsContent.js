import * as React from 'react';
import { TopImage } from '../TopImage';
import { AboutUsSection } from '../AboutUsSection';
import { Counter } from '../Counter';
import { SectionTitle } from '../SectionTitle';
import { TeamMembers } from '../TeamMembers';
import { Contact } from '../Contact';

export const AboutUsContent = ({ aboutUs, lang }) => {
  return (
    <>
      <TopImage title={aboutUs.title} />
      {aboutUs.sections.map(section => (
        <AboutUsSection
          key={section.title}
          title={section.title}
          subtitle={section.subtitle}
          description={section.description}
          image={section.image.img}
          alt={section.image.alt}
        />
      ))}
      <Counter lang={lang} />
      <section>
        <SectionTitle title={aboutUs.team.title} />
        <TeamMembers teamMembers={aboutUs.team.teamMembers} />
      </section>
      <Contact
        title={aboutUs.contact.title}
        description={aboutUs.contact.description}
      />
    </>
  );
};

import * as React from 'react';
import { Contact } from '../Contact';
import { MainSection } from '../MainSection';
import { TopImage } from '../TopImage';
import { ImageParagraph } from '../ImageParagraph';
import { SectionTitle } from '../SectionTitle';
import { PhotoGallery } from '../PhotoGallery';
import { PhotosSlideMobile } from '../PhotosSlideMobile';
import { Faq } from '../Faq';
import { SignUp } from '../CharacteristicsSignUp';
import { EnglishActivities } from '../EnglishActivities';

export const MagicSchoolContent = ({ magicSchool, lang }) => {
  return (
    <>
      <TopImage title={magicSchool.title} />
      <MainSection
        title={magicSchool.subtitle}
        description={magicSchool.mainParagraph}
        image={magicSchool.mainImage.img}
        alt={magicSchool.mainImage.alt}
      />
      <ImageParagraph
        image={magicSchool.secondImage.img}
        text={magicSchool.secondParagraph}
        alt={magicSchool.secondImage.alt}
      />
      <SignUp signUp={magicSchool.signUp} lang={lang} />
      <EnglishActivities activities={magicSchool.space} />
      <section>
        <SectionTitle title={magicSchool.faq.title} />
        <Faq questions={magicSchool.faq.questions} />
      </section>
      <section id="gallery">
        <SectionTitle title={magicSchool.gallery.title} />
        <PhotoGallery images={magicSchool.gallery.images} />
        <PhotosSlideMobile photos={magicSchool.gallery.images} />
      </section>
      <Contact
        title={magicSchool.contact.title}
        description={magicSchool.contact.description}
      />
    </>
  );
};

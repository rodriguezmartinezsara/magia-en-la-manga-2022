import * as React from 'react';
import { TopImage } from '../TopImage';
import { SectionTitle } from '../SectionTitle';
import { ServicesIcons } from '../ServicesIcons';
import { SectionItem } from '../SectionItem';
import { Contact } from '../Contact';

export const CorporateEventsContent = ({ corporateEvents }) => {
  return (
    <>
      <TopImage title={corporateEvents.title} />
      <section>
        <SectionTitle
          title={corporateEvents.subtitle}
          subtitle={corporateEvents.description}
        />
        <ServicesIcons services={corporateEvents.services} />
      </section>
      {corporateEvents.typeOfEvent.map(corporateEvent => (
        <SectionItem
          key={corporateEvent.title}
          title={corporateEvent.title}
          description={corporateEvent.description}
          characteristics={corporateEvent.characteristics}
          link={corporateEvent.link}
          image={corporateEvent.image.img}
          alt={corporateEvent.image.alt}
        />
      ))}

      <Contact
        title={corporateEvents.contact.title}
        description={corporateEvents.contact.description}
      />
    </>
  );
};

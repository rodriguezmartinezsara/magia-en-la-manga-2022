import * as React from 'react';
import parse from 'html-react-parser';
import { Link } from 'gatsby';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const ImageTitleDescription = ({ shows, lang = 'es' }) => {
  const imageTitleDescription = css`
    margin: 0 auto;
    padding: 0 24px 24px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 40px 32px;
    max-width: 450px;
    @media ${QUERIES.sm} {
      flex-flow: row wrap;
      max-width: 700px;
      padding: 0 32px 24px;
    }
    @media ${QUERIES.md} {
      gap: 48px 52px;
      padding: 0 52px 24px;
    }
    @media ${QUERIES.lg} {
      max-width: 1300px;
    }
    & > div {
      @media ${QUERIES.sm} {
        flex: 0 0 46%;
      }
      @media ${QUERIES.md} {
        flex-basis: 45%;
      }
      @media ${QUERIES.lg} {
        flex-basis: 29.5%;
      }
      &:only-child {
        @media ${QUERIES.sm} {
          flex: 1;
          display: flex;
          gap: 40px;
          align-items: center;
        }
        @media ${QUERIES.lg} {
          max-width: 800px;
        }
        > div {
          flex: 1;
        }
        h3 {
          @media ${QUERIES.sm} {
            padding-top: 0;
          }
        }
      }
    }
    .img {
      a {
        @media (hover: hover) and (prefers-reduced-motion: no-preference) {
          &:hover img,
          &:focus img {
            transform: scale(1.08);
            box-shadow: var(--box-shadow);
            transition: transform 300ms, box-shadow 300ms;
          }
        }
      }
      img {
        border-radius: 10px;
        width: 100%;
        height: auto;
        transform-origin: 50% 65%;
        transition: transform 700ms, box-shadow 600ms;
      }
    }
    h3 {
      font-size: 1.9rem;
      font-weight: 600;
      padding: 24px 0 16px 0;
    }
    p {
      padding-bottom: 16px;
    }
    a {
      text-decoration: none;
      color: var(--color-grey-300);
      transition: color 0.4s;
      display: block;
      @media (hover: hover) and (pointer: fine) {
        &:hover {
          color: var(--color-primary-dark);
        }
      }
    }
  `;

  return (
    <div css={imageTitleDescription} className="team">
      {shows.map(show => (
        <div key={show.title}>
          <div className="img">
            <Link to={show.slug}>
              <GatsbyImage
                image={show.smallImage.img.childImageSharp.gatsbyImageData}
                alt={show.smallImage.alt}
                style={{ overflow: 'inherit' }}
              />
            </Link>
          </div>
          <div className="info">
            <h3>{show.title}</h3>
            <p>{parse(show.synopsis)}</p>
            <Link to={show.slug}>
              {lang === 'es' ? 'Saber más...' : 'Saber máis...'}
            </Link>
          </div>
        </div>
      ))}
    </div>
  );
};

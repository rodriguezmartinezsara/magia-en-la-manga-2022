import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const ImageTitleDescriptionWorkshops = ({ workshops }) => {
  const imageTitleDescriptionWorkshops = css`
    margin: 0 auto;
    padding: 0 24px 24px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 32px;
    max-width: 450px;
    @media ${QUERIES.sm} {
      flex-flow: row wrap;
      max-width: 700px;
      gap: 40px 32px;
      padding: 0 32px 24px;
    }
    @media ${QUERIES.md} {
      gap: 48px 52px;
      padding: 0 52px 24px;
    }
    @media ${QUERIES.lg} {
      max-width: 1300px;
    }
    & > div {
      @media ${QUERIES.sm} {
        flex: 0 0 46%;
      }
      @media ${QUERIES.md} {
        flex-basis: 45%;
      }
      @media ${QUERIES.lg} {
        flex: 1;
      }
    }
    img {
      border-radius: 10px;
      width: 100%;
      height: auto;
    }
    h3 {
      font-size: 1.9rem;
      font-weight: 600;
      padding: 24px 0 16px 0;
    }
  `;

  return (
    <div css={imageTitleDescriptionWorkshops}>
      {workshops.map(workshop => (
        <div key={workshop.title}>
          <GatsbyImage
            image={workshop.smallImage.img.childImageSharp.gatsbyImageData}
            alt={workshop.smallImage.alt}
          />
          <h3>{workshop.title}</h3>
          <p>{parse(workshop.synopsis)}</p>
        </div>
      ))}
    </div>
  );
};

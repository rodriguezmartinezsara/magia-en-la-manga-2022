import * as React from 'react';
import { useState } from 'react';
import { slide as Menu } from 'react-burger-menu';
import { Link } from 'gatsby';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const MobileMenu = ({ menuLinks, translationSlug, lang }) => {
  const styles = {
    bmBurgerButton: {
      position: 'fixed',
      width: '20px',
      height: '18px',
      right: '20px',
      top: '23px',
    },
    bmBurgerBars: {
      background: 'white',
      height: '2px',
    },
    bmBurgerBarsHover: {
      background: '#EFD384',
    },
    bmCrossButton: {
      height: '24px',
      width: '24px',
      right: '20px',
      top: '20px',
    },
    bmCross: {
      background: '#bdc3c7',
      height: '24px',
      width: '2px',
      top: '-6px',
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%',
    },
    bmMenu: {
      background: 'hsl(70deg 5% 17% )',
      padding: '50px 20px',
    },
    bmMorphShape: {
      fill: '#373a47',
    },
    bmItemList: {},
    bmItem: {
      color: 'white',
      textDecoration: 'none',
    },
    bmOverlay: {
      background: 'rgba(0, 0, 0, 0.3)',
    },
  };

  const menu = css`
    @media ${QUERIES.lg} {
      display: none;
    }
    .bm-burger-bars {
      transition: background 0.4s;
    }
    .bm-burger-bars-hover {
      background: var(--color-primary) !important;
      opacity: 1 !important;
    }
    ul {
      font-size: 1.15rem;
      font-weight: 700;
      list-style: none;
      padding: 0;
      li {
        position: relative;
        padding: 6px 16px;
        &:not(:last-of-type) {
          border-bottom: 1px solid var(--color-grey-400);
        }
        a {
          text-decoration: none;
          color: white;
          transition: color 0.5s ease;
          display: block;
          &.active {
            color: var(--color-primary);
          }
          @media (hover: hover) and (pointer: fine) {
            &:hover {
              color: var(--color-primary);
            }
          }
        }
        .submenu {
          list-style: none;
          padding: 0;
          li {
            margin: 0 6px;
            font-size: 1rem;
            border-bottom: none;
            border-top: 1px solid var(--color-grey-400);
            padding: 4px 10px;
            &:first-of-type {
              margin-top: 6px;
            }
            &:last-of-type {
              padding-bottom: 0;
            }
          }
        }
      }
      .translation {
        font-size: 1rem;
        padding-top: 16px;
      }
    }
  `;

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <div css={menu}>
      <Menu
        right
        isOpen={isMenuOpen}
        onStateChange={state => setIsMenuOpen(state.isOpen)}
        styles={styles}
        width={250}
      >
        <ul>
          {menuLinks.map(link => (
            <li key={link.name}>
              <Link
                to={link.link}
                onClick={() => setIsMenuOpen(false)}
                activeClassName="active"
              >
                {link.name}
              </Link>
              {link.subMenu && (
                <ul className="submenu" aria-label="submenu">
                  {link.subMenu.map(subLink => (
                    <li key={subLink.name}>
                      <Link
                        to={subLink.link}
                        onClick={() => setIsMenuOpen(false)}
                        activeClassName="active"
                      >
                        {subLink.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              )}
            </li>
          ))}
          {translationSlug && (
            <li className="translation">
              <Link to={translationSlug}>{lang === 'es' ? 'GL' : 'ES'}</Link>
            </li>
          )}
        </ul>
      </Menu>
    </div>
  );
};

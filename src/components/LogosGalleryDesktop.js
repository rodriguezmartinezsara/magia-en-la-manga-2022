import * as React from 'react';
import { GatsbyImage } from 'gatsby-plugin-image';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const LogosGalleryDesktop = ({ logos }) => {
  const logosGalleryDesktop = css`
    display: none;
    @media ${QUERIES.lg} {
      max-width: 1250px;
      margin: 0 auto;
      padding: 0 32px 54px;
      display: grid;
      grid-template-columns: repeat(5, 1fr);
      gap: 24px;
      justify-items: center;
      align-items: center;
      img {
        width: 100%;
        height: auto;
      }
    }
  `;

  return (
    <div css={logosGalleryDesktop}>
      {logos.map(logo => (
        <GatsbyImage
          key={logo.alt}
          alt={logo.alt}
          image={logo.img.childImageSharp.gatsbyImageData}
        />
      ))}
    </div>
  );
};

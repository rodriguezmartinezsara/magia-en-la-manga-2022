import * as React from "react";
import { css } from "@emotion/react";

//Animation inspired by BrandMills Studios' design:
//https://dribbble.com/shots/3986110-2-Dribbble-Invitation

export const TopHatAnimation = () => {
  const topHat = css`
    width: 100%;
    height: auto;
    @media (prefers-reduced-motion: no-preference) {
      #hat,
      #hatBackground,
      #shadow {
        animation: shrinkHat 4s linear infinite;
        transform-origin: bottom;
        transform-box: fill-box;
      }
      @keyframes shrinkHat {
        20%,
        40%,
        82%,
        96% {
          transform: scale(1, 1);
        }
        28% {
          transform: scale(1.2, 0.8);
        }
        32%,
        92% {
          transform: scale(0.9, 1.1);
        }
        36% {
          transform: scale(1.15, 0.9);
        }
        88% {
          transform: scale(1.25, 0.9);
        }
      }
      #magicWand {
        animation: waveWand 4s linear infinite;
        transform-origin: 25% 75%;
        transform-box: fill-box;
      }
      @keyframes waveWand {
        0% {
          transform: translate(0px, 0px) rotate(0deg);
        }
        4% {
          transform: rotate(40deg);
        }
        10% {
          transform: rotate(11deg);
        }
        12% {
          transform: translate(-2px, -37px);
        }
        15% {
          transform: translate(-2px, -30px) rotate(-14deg);
        }
        20% {
          transform: rotate(15deg);
        }
        30% {
          transform: rotate(0deg);
        }
        68% {
          transform: translate(1px, 3px);
        }
        76% {
          transform: rotate(3deg);
        }
        100% {
          transform: rotate(-3deg);
        }
      }
      #sparkle1,
      #sparkle2,
      #sparkle3,
      #sparkle4 {
        animation: sparkleMovement 4s linear infinite;
        transform-origin: center;
        transform-box: fill-box;
      }
      #sparkle1 {
        animation-delay: 150ms;
      }
      #sparkle2 {
        animation-delay: 250ms;
      }
      #sparkle4 {
        animation-delay: 400ms;
      }
      @keyframes sparkleMovement {
        0%,
        40% {
          transform: rotate(0deg) scale(0, 0);
        }
        45% {
          transform: rotate(-30deg) scale(1, 1);
        }
        75% {
          transform: rotate(-120deg) scale(1, 1);
        }
        85%,
        100% {
          transform: rotate(-150deg) scale(0, 0);
        }
      }
      #firstNumber {
        animation: numberScale 4s linear 280ms infinite;
        transform-origin: 50% 80%;
        transform-box: fill-box;
      }
      #secondNumber {
        animation: numberScale 4s linear infinite;
        transform-origin: 50% 30%;
        transform-box: fill-box;
      }
      #thirdNumber {
        animation: numberScale 4s linear 150ms infinite;
        transform-origin: 50% 40%;
        transform-box: fill-box;
      }
      @keyframes numberScale {
        0% {
          transform: translate(0, 0) rotate(0deg) scale(0.4, 0.4);
        }
        20% {
          transform: translate(-1px, -1px) rotate(-2deg) scale(0.4, 0.4);
        }
        40% {
          transform: translate(1px, -1px) rotate(5deg) scale(1, 1);
        }
        50% {
          transform: translate(1px, 1px) rotate(1deg) scale(1, 1);
        }
        60% {
          transform: translate(-1px, 1px) rotate(-2deg) scale(1, 1);
        }
        80% {
          transform: translate(-1px, -1px) rotate(5deg) scale(1, 1);
        }
        90% {
          transform: translate(1px, -1px) rotate(-2deg) scale(0.4, 0.4);
        }
        100% {
          transform: translate(0, 0) rotate(0deg) scale(0.4, 0.4);
        }
      }
      #boxFirstNumber {
        animation: boxFirstNumberMovement 4s linear infinite;
        transform-origin: center;
        transform-box: fill-box;
      }
      @keyframes boxFirstNumberMovement {
        0% {
          transform: translate(30px, 85px);
        }
        29% {
          transform: translate(30px, 85px);
        }
        33% {
          transform: translate(20px, 25px);
        }
        42% {
          transform: translate(0px, 0px);
        }
        50% {
          transform: translate(0px, 0px);
        }
        60% {
          transform: translate(0px, 0px);
        }
        70% {
          transform: translate(0px, 0px);
        }
        84% {
          transform: translate(0px, 0px);
        }
        89% {
          transform: translate(24px, 45px);
        }
        91% {
          transform: translate(30px, 85px);
        }
        100% {
          transform: translate(30px, 85px);
        }
      }
      #boxSecondNumber {
        animation: boxSecondNumberMovement 4s linear infinite;
        transform-origin: center;
        transform-box: fill-box;
      }
      @keyframes boxSecondNumberMovement {
        0% {
          transform: translate(-1px, 85px);
        }
        28% {
          transform: translate(-1px, 85px);
        }
        33% {
          transform: translate(0px, 20px);
        }
        40% {
          transform: translate(0px, 0px);
        }
        50% {
          transform: translate(0px, 0px);
        }
        60% {
          transform: translate(0px, 0px);
        }
        70% {
          transform: translate(0px, 0px);
        }
        79% {
          transform: translate(0px, 0px);
        }
        83% {
          transform: translate(-5px, 45px);
        }
        85% {
          transform: translate(-8px, 85px);
        }
        100% {
          transform: translate(-8px, 85px);
        }
      }
      #boxThirdNumber {
        animation: boxThirdNumberMovement 4s linear infinite;
        transform-origin: center;
        transform-box: fill-box;
      }
      @keyframes boxThirdNumberMovement {
        0% {
          transform: translate(-30px, 85px);
        }
        30% {
          transform: translate(-30px, 85px);
        }
        34% {
          transform: translate(-20px, 25px);
        }
        44% {
          transform: translate(0px, 0px);
        }
        50% {
          transform: translate(0px, 0px);
        }
        60% {
          transform: translate(0px, 0px);
        }
        70% {
          transform: translate(0px, 0px);
        }
        81% {
          transform: translate(0px, 0px);
        }
        86% {
          transform: translate(-19px, 45px);
        }
        88% {
          transform: translate(-33px, 85px);
        }
        100% {
          transform: translate(-35px, 85px);
        }
      }
    }
  `;

  return (
    <svg css={topHat} version="1.1" id="topHat" viewBox="0 0 284 284">
      <circle
        id="backgroundCircle"
        fill="#F0D283"
        cx="141.7"
        cy="141.7"
        r="103.5"
      />
      <path
        id="shadow"
        fill="#757575"
        d="M142.4,212.7v8.9h73.8c3.9-2.8,6.5-5.7,10-8.9H142.4z"
      />
      <path
        id="backgroundFrame"
        fill="#FFFFFF"
        d="M141.7,22.6C76,22.6,22.6,76,22.6,141.7S76,260.8,141.7,260.8s119.1-53.3,119.1-119.1S207.4,22.6,141.7,22.6z M141.7,245.2c-57.2,0-103.5-46.3-103.5-103.5S84.5,38.2,141.7,38.2s103.5,46.3,103.5,103.5S198.9,245.2,141.7,245.2z"
      />
      <path
        id="sparkle1"
        fill="#FFFFFF"
        d="M79.4,157.9c-1.9,1.2-5.8,0.4-5.8,0.4s3.8,1.3,4.9,3.3c1,1.8,0.3,5.4,0.3,5.4s1.5-3.4,3.4-4.4c1.8-1,5.3-0.7,5.3-0.7s-3.2-1.2-4.3-2.9c-1.2-1.9-0.8-5.7-0.8-5.7S81.2,156.7,79.4,157.9z"
      />
      <path
        id="sparkle2"
        fill="#FFFFFF"
        d="M64.8,99.7c-1.8,2.5-7.2,3.3-7.2,3.3s5.4-0.3,7.8,1.7c2.2,1.9,3,6.7,3,6.7s0.2-5,2.1-7.3c1.8-2.2,6.3-3.5,6.3-3.5s-4.7,0-6.9-1.6c-2.5-1.8-3.9-6.8-3.9-6.8S66.5,97.3,64.8,99.7z"
      />
      <path
        id="sparkle3"
        fill="#FFFFFF"
        d="M163.7,60.2c-3.6,2.7-11.3,1.6-11.3,1.6s7.6,1.9,10.1,5.7c2.2,3.5,1.3,10.4,1.3,10.4s2.4-6.8,6-9.2c3.4-2.3,10.2-2.2,10.2-2.2s-6.5-1.9-8.9-5.1c-2.7-3.5-2.4-11.1-2.4-11.1S167.1,57.6,163.7,60.2z"
      />
      <path
        id="sparkle4"
        fill="#FFFFFF"
        d="M213.2,124.3c-2.2,0.7-5.7-1-5.7-1s3.4,2.1,4,4.3c0.5,2-1,5.3-1,5.3s2.2-2.9,4.3-3.5c2-0.6,5.3,0.5,5.3,0.5s-2.8-1.9-3.5-3.9c-0.8-2.1,0.6-5.7,0.6-5.7S215.3,123.6,213.2,124.3z"
      />
      <g id="magicWand">
        <path
          fill="#757575"
          d="M103.1,114.4L5.8,143.9c-2.4,0.7-2.3,3.7-1.8,5.4c0.5,1.8,2,4.1,4.4,3.4l97.3-29.5c2.4-0.7,3.3-2.2,2.5-5.4C107.3,114.6,105.5,113.7,103.1,114.4z"
        />
        <path
          fill="#FFFFFF"
          d="M86.5,125.2l21.9-6.6c-0.8-4.1-2.9-4.9-5.3-4.2l-18.2,5.5L86.5,125.2z"
        />
        <path
          fill="#474747"
          d="M86.5,125.2L4.3,150.1c1,2.2,2.5,3,4.1,2.6l79.2-24L86.5,125.2z"
        />
      </g>
      <g id="hatBackground">
        <path
          fill="#474747"
          d="M167.1,160.5l-13.2,1l-12.1,0.5l-12.1-0.5l-13.2-1l-12.9-2.6l4.1,56.2c0,4.4,15.3,8,34.1,8s34.1-3.6,34.1-8l4.1-56.2L167.1,160.5z"
        />
        <ellipse fill="#757575" cx="141.8" cy="144.8" rx="45.7" ry="15.2" />
        <ellipse
          id="hole"
          fill="#282828"
          cx="141.8"
          cy="144.8"
          rx="34"
          ry="9.2"
        />
      </g>
      <g id="boxThirdNumber">
        <path
          id="thirdNumber"
          fill="#7F1102"
          d="M197.8,96.6h-2.1V73.5c0-1.3-1-2.3-2.3-2.3h-0.7c-0.8,0-1.5,0.4-1.9,1L170.5,101c-0.5,0.7-0.6,1.6-0.2,2.4c0.4,0.8,1.2,1.2,2,1.2h15.1v6.2c0,1.3,1,2.3,2.3,2.3h3.6c1.3,0,2.3-1,2.3-2.3v-6.2h2.1c1.3,0,2.3-1,2.3-2.3v-3.5C200.1,97.6,199.1,96.6,197.8,96.6z M187.5,90.8v5.7h-4L187.5,90.8z"
        />
      </g>
      <g id="boxSecondNumber">
        <path
          id="secondNumber"
          fill="#7F1102"
          d="M157.4,83c-1.3-3.2-3.2-5.7-5.5-7.3c-2.4-1.7-5-2.5-7.9-2.5s-5.5,0.9-7.9,2.5c-2.3,1.7-4.1,4.1-5.3,7.2c-1.2,3-1.8,7-1.8,12.3c0,5.2,0.6,9.3,1.8,12.3c1.2,3.1,3,5.6,5.3,7.2c2.3,1.7,4.9,2.5,7.8,2.5c2.9,0,5.6-0.9,8-2.6c2.3-1.7,4.2-4.1,5.5-7.3c1.3-3.1,1.9-7,1.9-12.1S158.7,86.1,157.4,83z M147.4,107.9c-2,1.5-4.3,1.6-6.5,0c-1.1-0.8-1.9-2.1-2.5-3.8c-0.7-1.9-1-4.9-1-8.8s0.3-6.9,1-8.8c0.6-1.7,1.5-3,2.5-3.8s2.1-1.2,3.2-1.2c1.1,0,2.2,0.4,3.2,1.2c1.1,0.8,2,2.2,2.6,4.2c0.7,2.1,1.1,5,1.1,8.5s-0.4,6.4-1.3,8.8l0,0C149.2,105.8,148.4,107.1,147.4,107.9z"
        />
      </g>
      <g id="boxFirstNumber">
        <path
          id="firstNumber"
          fill="#7F1102"
          d="M115.3,90h-2.4V64c0-1.4-1.2-2.6-2.6-2.6h-0.8c-0.8,0-1.6,0.4-2.1,1.1L84.5,95c-0.6,0.8-0.6,1.8-0.2,2.7s1.3,1.4,2.3,1.4h17v6.9c0,1.4,1.2,2.6,2.6,2.6h4.1c1.4,0,2.6-1.2,2.6-2.6v-6.9h2.4c1.4,0,2.6-1.2,2.6-2.6v-3.9C117.9,91.1,116.7,90,115.3,90z M103.6,83.5V90H99L103.6,83.5z"
        />
      </g>
      <g id="hat">
        <path
          fill="#474747"
          d="M167.1,160.5l-13.2,1l-12.1,0.5l-12.1-0.5l-13.2-1l-12.9-2.6l4.1,56.2c0,4.4,15.3,8,34.1,8s34.1-3.6,34.1-8l4.1-56.2L167.1,160.5z"
        />
        <path
          fill="#282828"
          d="M141.7,180.3c0,0.2,0,0.3,0,0.5c0.1,8.8,0,33,0,41.3l0,0c18.8,0,34.1-3.6,34.1-8l2.9-40.1C170.4,177.8,157,180.3,141.7,180.3L141.7,180.3z"
        />
        <path
          fill="#FFFFFF"
          d="M116.4,160.5l-12.8-2.6l1.2,16.1c8.3,3.8,21.8,6.3,37,6.3c15.3,0,28.6-2.5,37-6.3l1.2-16.1l-12.8,2.6l-6.7,0.5l-6.6,0.5l-12.1,0.5l0,0l0,0l-12.1-0.5L116.4,160.5z"
        />
        <path
          fill="#E0E0E0"
          d="M168.1,160.3c-11.2,2.7-25.2,4.3-36.9,3.2c6.9,3.5,10.3,6.5,10.5,16.8c0,0,0,0,0.1,0c15.3,0,28.6-2.5,37-6.3l1.2-16.1L168.1,160.3z"
        />
        <path
          fill="#474747"
          d="M141.8,160c-25.2,0-45.7-6.8-45.7-15.2v5c0,8.4,20.5,15.2,45.7,15.2s45.7-6.8,45.7-15.2v-5C187.4,153.2,167.1,160,141.8,160z"
        />
        <path
          fill="#757575"
          d="M141.8,154c-18.8,0-34-4.1-34-9.2H96.1c0,8.4,20.5,15.2,45.7,15.2c25.2,0,45.7-6.8,45.7-15.2h-11.7C175.8,149.9,160.6,154,141.8,154z"
        />
        <ellipse fill="none" cx="141.8" cy="144.8" rx="45.7" ry="15.2" />
      </g>
    </svg>
  );
};

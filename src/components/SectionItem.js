import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const SectionItem = ({
  title,
  description,
  characteristics,
  image,
  alt,
  link,
}) => {
  const sectionItem = css`
    margin: 0 auto;
    padding: 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 16px;
    scroll-margin-top: 70px;
    @media ${QUERIES.sm} {
      padding: 24px 56px;
    }
    @media ${QUERIES.md} {
      flex-direction: row;
      max-width: 900px;
      padding: 24px 40px;
      gap: 40px;
      &:nth-of-type(odd) {
        flex-direction: row-reverse;
      }
    }
    @media ${QUERIES.lg} {
      max-width: 1300px;
      gap: 108px;
    }
    h3,
    p {
      text-align: center;
    }
    h3 {
      font-size: var(--fs-600);
      font-weight: 600;
      position: relative;
      padding-bottom: 16px;
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 0;
        right: 50%;
        transform: translateX(50%);
      }
    }
    p {
      padding: 16px 0 32px;
    }
    ul {
      list-style: none;
      li {
        color: var(--color-grey-300);
        position: relative;
        padding-bottom: 16px;
        &::before {
          //FIXME arreglar esto para que el color venga de una variable
          content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56.7 56.7' fill='%23F0D283' ><path d='M32.8,49c0.3,0.3,0,0.8-0.4,0.9c-1.6,0.3-2.5,0.5-4.1,0.8c-1.6,0.3-2.5,0.5-4.1,0.8 c-0.4,0.1-0.7-0.4-0.4-0.8c0.6-1,1.6-2.5,2.3-3.9c0.6-1.2,1.3-2.9,1.7-4.1c0.2-0.5,0.8-0.6,0.9-0.2c0.4,1,1.1,2.5,1.7,3.5 C31.2,47.1,32.2,48.3,32.8,49z M39.9,20.6C39.9,20.6,39.9,20.6,39.9,20.6c-0.3,0.1-2.6,0.6-2.8,0.2c-0.5-0.8,1.4-2.4,1.4-7.1 c0-5.8-4.5-9.5-10.1-8.4C22.8,6.4,18.3,12,18.3,17.7c0,4.7,1.9,5.6,1.4,6.6c-0.2,0.4-2.5,0.8-2.8,0.9c0,0,0,0,0,0 c-5.6,1.1-10.1,6.7-10.1,12.4c0,5.8,4.5,9.5,10.1,8.4c3.7-0.7,6.9-3.4,8.7-6.9c0,0,0,0,0,0c1.1-2.1,1.7-3.1,2.8-5.1 c1.1,1.6,1.7,2.4,2.8,4c0,0,0,0,0,0c1.8,2.7,5,4.2,8.7,3.4C45.5,40.3,50,34.8,50,29C50,23.3,45.5,19.5,39.9,20.6z'></path></svg>");
          width: 24px;
          height: 24px;
          position: absolute;
          left: -32px;
          top: 3px;
        }
      }
      @media ${QUERIES.lg} {
        padding: 0 80px;
      }
    }
    .img {
      @media ${QUERIES.sm} {
        padding: 0 64px;
      }
      @media ${QUERIES.md} {
        padding: 0;
        flex: 1;
      }
      @media ${QUERIES.lg} {
        flex: 3;
      }
      img {
        border-radius: 10px;
        width: 100%;
        height: auto;
        box-shadow: var(--box-shadow);
      }
    }
    .info {
      width: 100%;
      overflow-wrap: break-word;
      @media ${QUERIES.md} {
        flex: 1;
      }
      @media ${QUERIES.lg} {
        flex: 4;
      }
    }
  `;

  return (
    <section id={link.substring(1)} css={sectionItem}>
      <div className="info">
        <h3>{title}</h3>
        <p>{parse(description)}</p>
        <ul>
          {characteristics.map(characteristic => (
            <li key={characteristic.id}>{characteristic.item}</li>
          ))}
        </ul>
      </div>
      <div className="img">
        <GatsbyImage
          image={image.childImageSharp.gatsbyImageData}
          alt={alt}
          style={{ overflow: 'inherit' }}
        />
      </div>
    </section>
  );
};

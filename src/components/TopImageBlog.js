import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const TopImageBlog = ({ title, date, category, image }) => {
  const topImage = css`
    min-height: 200px;
    background: linear-gradient(
        0deg,
        rgba(60, 60, 60, 0.6),
        rgba(60, 60, 60, 0.6)
      ),
      url('/magia-en-la-manga-02.webp');
    background-position: center;
    background-size: cover;
    display: flex;
    align-items: center;
    justify-content: center;
    @media ${QUERIES.lg} {
      min-height: 300px;
    }
    .titleWrapper {
      margin-top: 66px;
      padding: 16px 16px 12px;
      text-align: center;
      color: var(--color-white);
      @media ${QUERIES.md} {
        padding: 32px 32px 20px;
      }
      h1 {
        padding-bottom: 8px;
        font-size: var(--fs-900);
        font-weight: 500;
        color: inherit;
      }
      .date {
        color: inherit;
      }
      .category {
        color: var(--color-primary);
        text-transform: uppercase;
      }
    }
  `;

  return (
    <section css={topImage}>
      <div className="titleWrapper">
        <h1>{title}</h1>
        {date && (
          <p className="date">
            {new Date(date).toLocaleDateString('es-ES', {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            })}
          </p>
        )}
        {category && <p className="category">{category}</p>}
      </div>
    </section>
  );
};

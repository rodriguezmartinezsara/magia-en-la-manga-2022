import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const ImageParagraph = ({ image, text, alt }) => {
  const imageParagraph = css`
    padding: 24px;
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    gap: 32px;
    max-width: 450px;
    align-items: center;
    @media ${QUERIES.sm} {
      max-width: 800px;
      padding: 24px 32px;
    }
    @media ${QUERIES.md} {
      flex-direction: row;
      gap: 48px;
    }
    @media ${QUERIES.lg} {
      max-width: 1250px;
      padding: 24px 48px;
      align-items: flex-end;
    }
    .text {
      width: 100%;
      @media ${QUERIES.md} {
        flex: 1;
      }
      @media ${QUERIES.lg} {
        flex: 2;
      }
      p:not(:last-of-type) {
        padding-bottom: 16px;
      }
      a {
        text-decoration: none;
        color: var(--color-grey-300);
        transition: color 0.4s;
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
    }
    .img {
      @media ${QUERIES.sm} {
        padding: 0 80px;
      }
      @media ${QUERIES.md} {
        flex: 1;
        padding: 0;
      }
      @media ${QUERIES.lg} {
        margin-top: -75px;
      }
      img {
        width: 100%;
        height: auto;
        border-radius: 10px;
        box-shadow: var(--box-shadow);
      }
    }
  `;

  return (
    <div css={imageParagraph}>
      <div className="img">
        <GatsbyImage
          image={image.childImageSharp.gatsbyImageData}
          alt={alt}
          style={{ overflow: 'inherit' }}
        />
      </div>
      <div className="text">{parse(text)}</div>
    </div>
  );
};

import * as React from 'react';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { css } from '@emotion/react';
import { MobileMenu } from './MobileMenu';
import { DesktopMenu } from './DesktopMenu';
import { QUERIES } from '../utils/constants';

const Header = ({ lang = 'es', translationSlug }) => {
  const getData = useStaticQuery(graphql`
    query SiteQuery {
      site {
        siteMetadata {
          title
          menuLinks {
            es {
              link
              name
              subMenu {
                link
                name
              }
            }
            gl {
              link
              name
              subMenu {
                link
                name
              }
            }
          }
        }
      }
    }
  `);

  const siteTitle = getData.site.siteMetadata.title;
  const menuLinks = getData.site.siteMetadata.menuLinks[lang];

  const [scrolled, setScrolled] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    const handleScroll = () => {
      const position = window.pageYOffset;
      setScrollPosition(position);
    };

    window.addEventListener('scroll', handleScroll, { passive: true });
    if (scrollPosition >= 100) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [scrollPosition]);

  return (
    <>
      <header css={header} className={scrolled ? 'scrolled' : ''}>
        <div className="wrapper">
          <div className="logo">
            <Link to={lang === 'es' ? '/' : '/gl/'}>
              <img
                src="/magia-en-la-manga-logo-claro.svg"
                alt="Logo Magia en la Manga"
                height="50"
                width="35.34"
              />
            </Link>
            <Link to={lang === 'es' ? '/' : '/gl/'}>{siteTitle}</Link>
          </div>
          <DesktopMenu
            menuLinks={menuLinks}
            translationSlug={translationSlug}
            lang={lang}
          />
        </div>
      </header>
      <MobileMenu
        menuLinks={menuLinks}
        translationSlug={translationSlug}
        lang={lang}
      />
    </>
  );
};

const header = css`
  background-color: rgba(255, 255, 255, 0.1);
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 999;
  padding: 6px 12px;
  min-height: 58px;
  transition: all 0.2s linear;
  &.scrolled {
    background-color: var(--color-menu);
    backdrop-filter: blur(20px) saturate(180%);
  }
  @media ${QUERIES.sm} {
    padding: 6px 24px;
  }
  .wrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1400px;
    margin: 0 auto;
  }
  .logo {
    text-transform: uppercase;
    font-size: 18px;
    display: flex;
    align-items: center;
    gap: 6px;
    @media ${QUERIES.sm} {
      font-size: 28px;
      gap: 10px;
    }
    img {
      height: 50px;
      width: auto;
    }
    a {
      text-decoration: none;
      color: white;
    }
  }
`;

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;

import * as React from 'react';
import { css } from '@emotion/react';
import { GatsbyImage } from 'gatsby-plugin-image';
import { QUERIES } from '../utils/constants';
import parse from 'html-react-parser';

export const EnglishActivities = ({ activities }) => {
  const englishActivities = css`
    padding: 24px;
    margin: 0 auto;
    max-width: 450px;
    align-items: center;
    @media ${QUERIES.sm} {
      max-width: 800px;
      padding: 24px 32px;
    }
    @media ${QUERIES.lg} {
      max-width: 1250px;
      padding: 24px 48px;
    }
    .col {
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 24px;
      @media ${QUERIES.md} {
        flex-direction: row;
        gap: 48px;
      }
      & > div {
        flex: 1;
        width: 100%;
      }
    }
    .col.video {
      flex-direction: column-reverse;
      &:not(:only-child) {
        padding-bottom: 48px;
      }
      @media ${QUERIES.md} {
        flex-direction: row;
      }
      h3 {
        font-size: var(--fs-400);
        font-weight: 500;
        position: relative;
        padding-bottom: 16px;
        &::after {
          content: '';
          background-color: var(--color-primary);
          width: 60px;
          height: 2px;
          position: absolute;
          bottom: 0;
          left: 50px;
        }
      }
      p {
        padding-top: 16px;
      }
      ul {
        list-style: none;
        li {
          position: relative;
          padding-top: 8px;
          &::before {
            content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56.7 56.7' fill='%23F0D283' ><path d='M32.8,49c0.3,0.3,0,0.8-0.4,0.9c-1.6,0.3-2.5,0.5-4.1,0.8c-1.6,0.3-2.5,0.5-4.1,0.8 c-0.4,0.1-0.7-0.4-0.4-0.8c0.6-1,1.6-2.5,2.3-3.9c0.6-1.2,1.3-2.9,1.7-4.1c0.2-0.5,0.8-0.6,0.9-0.2c0.4,1,1.1,2.5,1.7,3.5 C31.2,47.1,32.2,48.3,32.8,49z M39.9,20.6C39.9,20.6,39.9,20.6,39.9,20.6c-0.3,0.1-2.6,0.6-2.8,0.2c-0.5-0.8,1.4-2.4,1.4-7.1 c0-5.8-4.5-9.5-10.1-8.4C22.8,6.4,18.3,12,18.3,17.7c0,4.7,1.9,5.6,1.4,6.6c-0.2,0.4-2.5,0.8-2.8,0.9c0,0,0,0,0,0 c-5.6,1.1-10.1,6.7-10.1,12.4c0,5.8,4.5,9.5,10.1,8.4c3.7-0.7,6.9-3.4,8.7-6.9c0,0,0,0,0,0c1.1-2.1,1.7-3.1,2.8-5.1 c1.1,1.6,1.7,2.4,2.8,4c0,0,0,0,0,0c1.8,2.7,5,4.2,8.7,3.4C45.5,40.3,50,34.8,50,29C50,23.3,45.5,19.5,39.9,20.6z'></path></svg>");
            width: 24px;
            height: 24px;
            position: absolute;
            left: -32px;
            top: 11px;
          }
        }
      }
      .iframe-video-wrapper {
        position: relative;
        width: 100%;
        aspect-ratio: 16/9;
        iframe {
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 100%;
          border: none;
        }
      }
    }
    .col.image {
      .img-wrapper {
        @media ${QUERIES.sm} {
          padding: 0 80px;
        }
        @media ${QUERIES.md} {
          padding: 0;
          flex: 2;
        }
        img {
          width: 100%;
          height: auto;
          border-radius: 10px;
          box-shadow: var(--box-shadow);
        }
      }
      .text {
        @media ${QUERIES.md} {
          flex: 3;
        }
      }
    }
  `;

  return (
    <section css={englishActivities}>
      <div className="col video">
        <div>
          <h3>{activities.title}</h3>
          <p>{parse(activities.mainParagraph)}</p>
          {activities.characteristics.title && (
            <p>{activities.characteristics.title}</p>
          )}
          <ul>
            {activities.characteristics.items.map(item => (
              <li key={item.id}>{item.text}</li>
            ))}
          </ul>
        </div>
        <div>
          <div className="iframe-video-wrapper">
            <iframe
              src={activities.video.url}
              title={activities.video.title}
              allowFullScreen
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            />
          </div>
        </div>
      </div>
      {activities.image && (
        <div className="col image">
          <div className="img-wrapper">
            <GatsbyImage
              image={activities.image.img.childImageSharp.gatsbyImageData}
              alt={activities.image.alt}
              style={{ overflow: 'inherit' }}
            />
          </div>
          <div className="text">
            <p>{parse(activities.secondParagraph)}</p>
          </div>
        </div>
      )}
    </section>
  );
};

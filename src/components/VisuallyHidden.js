import * as React from "react";
import { css } from "@emotion/react";

// Stolen from https://www.joshwcomeau.com/snippets/react-components/visually-hidden/

const visuallyHidden = css`
  display: inline-block;
  position: absolute;
  overflow: hidden;
  clip: rect(0 0 0 0);
  height: 1px;
  width: 1px;
  margin: -1px;
  padding: 0;
  border: 0;
`;

export const VisuallyHidden = ({ children, ...delegated }) => {
  // const [forceShow, setForceShow] = React.useState(false);
  // React.useEffect(() => {
  //   if (process.env.NODE_ENV !== 'production') {
  //     const handleKeyDown = ev => {
  //       if (ev.key === 'Alt') {
  //         setForceShow(true);
  //       }
  //     };
  //     const handleKeyUp = ev => {
  //       if (ev.key === 'Alt') {
  //         setForceShow(false);
  //       }
  //     };
  //     window.addEventListener('keydown', handleKeyDown);
  //     window.addEventListener('keyup', handleKeyUp);
  //     return () => {
  //       window.removeEventListener('keydown', handleKeyDown);
  //       window.removeEventListener('keyup', handleKeyUp);
  //     };
  //   }
  // }, []);
  // if (forceShow) {
  //   return children;
  // }

  return (
    <span css={visuallyHidden} {...delegated}>
      {children}
    </span>
  );
};

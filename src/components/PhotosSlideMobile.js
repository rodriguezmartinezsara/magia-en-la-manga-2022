import React, { useEffect, useState } from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import { GatsbyImage } from 'gatsby-plugin-image';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { Gallery, Item } from 'react-photoswipe-gallery';
import 'photoswipe/dist/photoswipe.css';

export const PhotosSlideMobile = ({ photos }) => {
  //cosa que me mandó poner el capullo de Carlos y que no entiendo
  //para solucionar un error horrible que dice así: "Uncaught Error: Minified React error"
  //https://stackoverflow.com/a/72318597/2365348
  const [domLoaded, setDomLoaded] = useState(false);
  useEffect(() => {
    setDomLoaded(true);
  }, []);

  const logoSlideMobile = css`
    padding: 0 8px 16px;
    @media ${QUERIES.md} {
      display: none;
    }
    img {
      width: 100%;
      height: auto;
      padding: 0 8px;
      cursor: pointer;
    }
    ul.indicators {
      padding: 0;
    }
  `;

  const responsiveSettings = [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ];
  const buttonStyle = {
    width: '65px',
    background: 'none',
    border: '0px',
    padding: 10,
    cursor: 'pointer',
  };
  const properties = {
    prevArrow: (
      <button style={{ marginRight: -65, ...buttonStyle }}>
        <svg viewBox="0 0 60 60" fill="#fff">
          <path
            stroke="#000000"
            strokeWidth="0.3"
            d="M29 43l-3 3-16-16 16-16 3 3-13 13 13 13z"
          />
        </svg>
      </button>
    ),
    nextArrow: (
      <button style={{ marginLeft: -65, ...buttonStyle }}>
        <svg viewBox="0 0 60 60" fill="#fff">
          <path
            stroke="#000000"
            strokeWidth="0.3"
            d="M31,43l3,3l16-16L34,14l-3,3l13,13L31,43z"
          />
        </svg>
      </button>
    ),
  };

  return (
    <>
      {domLoaded && (
        <Gallery>
          <Slide
            {...properties}
            css={logoSlideMobile}
            slidesToScroll={1}
            slidesToShow={1}
            responsive={responsiveSettings}
            duration={2000}
            indicators={true}
            easing={'ease'}
          >
            {photos.map(photo => (
              <Item
                original={
                  photo.img.childImageSharp.gatsbyImageData.images.fallback.src
                }
                thumbnail={
                  photo.img.childImageSharp.gatsbyImageData.images.fallback.src
                }
                cropped
                width={photo.width}
                height={photo.height}
                key={photo.id}
              >
                {({ ref, open }) => (
                  <div
                    onClick={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      open();
                    }}
                    ref={ref}
                  >
                    <GatsbyImage
                      alt={photo.alt}
                      image={photo.img.childImageSharp.gatsbyImageData}
                    />
                  </div>
                )}
              </Item>
            ))}
          </Slide>
        </Gallery>
      )}
    </>
  );
};

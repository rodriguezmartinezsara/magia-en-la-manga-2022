import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const SignUp = ({ signUp, lang = 'es' }) => {
  const signUpSection = css`
    padding: 32px 24px;
    margin: 0 auto;
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 48px;
    max-width: 450px;
    @media ${QUERIES.sm} {
      max-width: 1200px;
      padding: 32px;
    }
    @media ${QUERIES.md} {
      flex-direction: row;
      padding: 40px 32px;
    }
    @media ${QUERIES.lg} {
      gap: 96px;
      padding: 56px 48px 48px;
    }
    ul {
      list-style: none;
      li {
        padding-bottom: 8px;
        position: relative;
      }
    }
    .characteristics {
      background-color: var(--color-grey-100);
      position: relative;
      padding: 32px 24px 24px;
      flex: 1;
      width: 100%;
      overflow-wrap: break-word;
      @media ${QUERIES.lg} {
        flex: 3;
      }
      &::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 0 100%, 100% 100%);
        top: -16px;
        left: 0;
      }
      &::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 100% 0, 100% 100%);
        bottom: -16px;
        left: 0;
      }
      li {
        &::before {
          //FIXME arreglar esto para que el color venga de una variable
          content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 283.5 283.5' fill='%2384909e' ><path d='M141.7,250.9c-60.2,0-109.2-49-109.2-109.2c0-60.2,49-109.2,109.2-109.2c60.2,0,109.2,49,109.2,109.2 C250.9,202,202,250.9,141.7,250.9z M141.7,52.5c-49.2,0-89.2,40-89.2,89.2s40,89.2,89.2,89.2s89.2-40,89.2-89.2 S190.9,52.5,141.7,52.5z M190,123.4c3.9-3.9,3.9-10.2,0-14.1c-3.9-3.9-10.2-3.9-14.1,0l-45.2,45.2l-20.9-22.6 c-3.8-4.1-10.1-4.3-14.1-0.5s-4.3,10.1-0.5,14.1l35.1,37.8L190,123.4z'></path></svg>");
          width: 24px;
          height: 24px;
          position: absolute;
          left: -32px;
          top: 2px;
        }
      }
    }
    .signUp {
      flex: 1;
      @media ${QUERIES.sm} {
        padding: 0 80px;
      }
      @media ${QUERIES.md} {
        padding: 0;
      }
      @media ${QUERIES.lg} {
        flex: 2;
      }
      h3 {
        font-size: var(--fs-400);
        font-weight: 500;
        position: relative;
        padding-bottom: 16px;
        &::after {
          content: '';
          background-color: var(--color-primary);
          width: 60px;
          height: 2px;
          position: absolute;
          bottom: 0;
          left: 50px;
        }
      }
      p {
        padding: 12px 0 32px;
      }
      .button {
        padding: 8px 16px;
        font-weight: 700;
        border-radius: 5px;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        transition: background-color 0.4s, color 0.4s;
        color: black;
        background-color: var(--color-primary);
        @media ${QUERIES.md} {
          padding: 16px 24px;
        }
        &:hover {
          background-color: var(--color-primary-dark);
        }
      }
    }
  `;

  return (
    <section css={signUpSection}>
      <div className="characteristics">
        <ul>
          {signUp.items.map(item => (
            <li key={item.id}>{item.text}</li>
          ))}
        </ul>
      </div>

      <div className="signUp">
        <h3>{signUp.title}</h3>
        <p>{signUp.subtitle}</p>
        <a href={signUp.link} className="button" target="_blank">
          {lang === 'es' ? 'Inscripciones' : 'Inscricións'}
        </a>
      </div>
    </section>
  );
};

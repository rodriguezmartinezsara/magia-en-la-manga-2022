import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const AboutUsSection = ({
  title,
  subtitle,
  description,
  image,
  alt,
}) => {
  const aboutUsSection = css`
    text-align: center;
    margin: 0 auto;
    padding: 24px;
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 24px;
    @media ${QUERIES.sm} {
      max-width: 800px;
      margin: 0 auto;
      gap: 48px;
      padding: 24px 32px;
      &:nth-of-type(odd) {
        flex-direction: row;
        .img {
          flex: 2;
        }
        .info {
          flex: 3;
        }
      }
    }
    @media ${QUERIES.md} {
      padding: 48px 32px;
    }
    @media ${QUERIES.lg} {
      max-width: 1300px;
      flex-direction: row;
      text-align: left;
      padding: 48px;
      .img,
      .info {
        flex: 1;
      }
      &:nth-of-type(odd) {
        flex-direction: row-reverse;
        gap: 160px;
      }
    }
    .info {
      width: 100%;
    }
    img {
      width: 100%;
      height: auto;
      border-radius: 10px;
      background-color: var(--color-grey-700);
      box-shadow: var(--box-shadow);
      color: var(--color-primary);
    }
    h2 {
      font-size: 1.5rem;
      padding: 24px 0;
      font-weight: 700;
    }
    span {
      font-family: 'Josefin Sans', sans-serif;
      font-weight: 700;
      color: var(--color-grey-300);
      display: block;
      position: relative;
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: -10px;
        right: 50%;
        transform: translateX(50%);
      }
      @media ${QUERIES.lg} {
        &::after {
          content: '';
          background-color: var(--color-primary);
          width: 60px;
          height: 2px;
          position: absolute;
          bottom: -10px;
          left: 0;
          transform: translateX(0);
        }
      }
    }
    p:not(:first-of-type) {
      padding-top: 16px;
    }
  `;

  return (
    <section css={aboutUsSection}>
      <div className="info">
        <span>{title}</span>
        <h2>{subtitle}</h2>
        {parse(description)}
      </div>
      <div className="img">
        <GatsbyImage
          alt={alt}
          image={image.childImageSharp.gatsbyImageData}
          style={{ overflow: 'inherit' }}
        />
      </div>
    </section>
  );
};

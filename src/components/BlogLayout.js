import * as React from 'react';
import { useState } from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { BlogPostsPreview } from './BlogPostsPreview';

export const BlogLayout = ({ children }) => {
  const getData = useStaticQuery(graphql`
    query GetBlogLayoutData {
      recentPosts: allPostsJson(sort: { fields: date, order: DESC }, limit: 3) {
        nodes {
          id
          title
          slug
        }
      }
      allCategories: allPostsJson {
        nodes {
          category
        }
      }
    }
  `);

  const [searchInput, setSearchInput] = useState('');
  const [selectedCategory, setSelectedCategory] = useState('');

  const recentPosts = getData.recentPosts.nodes;
  const uniqueCategories = Array.from(
    new Set(getData.allCategories.nodes.map(e => e.category))
  );

  const blogLayout = css`
    background-color: var(--color-grey-100);
    & > div {
      margin: 0 auto;
      display: grid;
      padding: 48px 24px;
      gap: 48px;
      @media ${QUERIES.sm} {
        padding: 48px 32px;
      }
      @media ${QUERIES.md} {
        padding: 48px;
      }
      @media ${QUERIES.lg} {
        padding: 72px 48px;
        grid-template-columns: 3fr 1fr;
        max-width: 1300px;
        gap: 68px;
      }
    }
    aside {
      max-width: 450px;
      position: relative;
      input {
        width: 100%;
        padding: 8px 32px 8px 8px;
        margin-bottom: 4px;
      }
      button.reset {
        position: absolute;
        top: 5px;
        right: 5px;
        border-color: transparent;
        background-color: transparent;
        cursor: pointer;
      }
      a {
        text-decoration: none;
        transition: color 0.4s;
        color: inherit;
        display: block;
        padding: 6px 0;
        cursor: pointer;
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
      h3 {
        text-transform: uppercase;
        padding: 32px 0 8px;
        font-size: 1.1rem;
        font-weight: 700;
      }
      button.category {
        border: none;
        background-color: transparent;
        transition: color 0.4s;
        color: inherit;
        display: block;
        padding: 4px 0;
        cursor: pointer;
        &.selected {
          color: var(--color-primary-dark);
        }
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
    }
  `;

  const handleChangeSearchInput = e => {
    setSearchInput(e.target.value);
    setSelectedCategory('');
  };
  const clearButton = () => {
    setSearchInput('');
  };
  const handleCategoryClick = category => () => {
    setSelectedCategory(selectedCategory === category ? '' : category);
    setSearchInput('');
  };

  return (
    <section css={blogLayout}>
      <div>
        {children && !searchInput && !selectedCategory ? (
          children
        ) : (
          <BlogPostsPreview
            searchInput={searchInput}
            setSearchInput={setSearchInput}
            category={selectedCategory}
            setCategory={setSelectedCategory}
          />
        )}
        <aside>
          <input
            onChange={handleChangeSearchInput}
            value={searchInput}
            placeholder="Buscar"
            aria-label="Buscar en el blog"
          />
          {searchInput && (
            <button className="reset" onClick={clearButton}>
              ✖
            </button>
          )}
          <h3>Entradas recientes</h3>
          {recentPosts.map(recentPost => (
            <Link to={`${recentPost.slug}`} key={recentPost.id}>
              {recentPost.title}
            </Link>
          ))}
          <h3>Categorías</h3>
          {uniqueCategories.map(category => (
            <button
              className={
                category === selectedCategory ? 'category selected' : 'category'
              }
              key={category}
              onClick={handleCategoryClick(category)}
            >
              {category}
            </button>
          ))}
        </aside>
      </div>
    </section>
  );
};

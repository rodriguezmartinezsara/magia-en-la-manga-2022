import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { GatsbyImage } from 'gatsby-plugin-image';

export const CharacteristicsGoals = ({
  characteristics,
  goalsTitle,
  goalsItems,
}) => {
  const characteristicsGoals = css`
    padding: 32px 24px;
    margin: 0 auto;
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 48px;
    max-width: 450px;
    @media ${QUERIES.sm} {
      max-width: 1200px;
      padding: 32px;
    }
    @media ${QUERIES.md} {
      flex-direction: row;
    }
    @media ${QUERIES.lg} {
      gap: 96px;
      padding: 32px 48px;
    }
    h4 {
      font-size: 1.5rem;
      padding-bottom: 24px;
      padding-left: 16px;
    }
    ul {
      list-style: none;
      li {
        padding-bottom: 8px;
        position: relative;
      }
    }
    .characteristics {
      background-color: var(--color-grey-100);
      position: relative;
      padding: 32px 24px 24px;
      flex: 1;
      width: 100%;
      overflow-wrap: break-word;
      @media ${QUERIES.lg} {
        flex: 3;
      }
      &::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 0 100%, 100% 100%);
        top: -16px;
        left: 0;
      }
      &::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 16px;
        background-color: var(--color-grey-100);
        clip-path: polygon(0 0, 100% 0, 100% 100%);
        bottom: -16px;
        left: 0;
      }
      li {
        &::before {
          //FIXME arreglar esto para que el color venga de una variable
          content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 283.5 283.5' fill='%2384909e' ><path d='M141.7,250.9c-60.2,0-109.2-49-109.2-109.2c0-60.2,49-109.2,109.2-109.2c60.2,0,109.2,49,109.2,109.2 C250.9,202,202,250.9,141.7,250.9z M141.7,52.5c-49.2,0-89.2,40-89.2,89.2s40,89.2,89.2,89.2s89.2-40,89.2-89.2 S190.9,52.5,141.7,52.5z M190,123.4c3.9-3.9,3.9-10.2,0-14.1c-3.9-3.9-10.2-3.9-14.1,0l-45.2,45.2l-20.9-22.6 c-3.8-4.1-10.1-4.3-14.1-0.5s-4.3,10.1-0.5,14.1l35.1,37.8L190,123.4z'></path></svg>");
          width: 24px;
          height: 24px;
          position: absolute;
          left: -32px;
          top: 2px;
        }
      }
    }
    .goals {
      width: 100%;
      flex: 1;
      overflow-wrap: break-word;
      @media ${QUERIES.lg} {
        flex: 4;
      }
      h4 {
        text-align: center;
      }
      li {
        color: var(--color-grey-300);
        &::before {
          //FIXME arreglar esto para que el color venga de una variable
          content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56.7 56.7' fill='%23F0D283' ><path d='M32.8,49c0.3,0.3,0,0.8-0.4,0.9c-1.6,0.3-2.5,0.5-4.1,0.8c-1.6,0.3-2.5,0.5-4.1,0.8 c-0.4,0.1-0.7-0.4-0.4-0.8c0.6-1,1.6-2.5,2.3-3.9c0.6-1.2,1.3-2.9,1.7-4.1c0.2-0.5,0.8-0.6,0.9-0.2c0.4,1,1.1,2.5,1.7,3.5 C31.2,47.1,32.2,48.3,32.8,49z M39.9,20.6C39.9,20.6,39.9,20.6,39.9,20.6c-0.3,0.1-2.6,0.6-2.8,0.2c-0.5-0.8,1.4-2.4,1.4-7.1 c0-5.8-4.5-9.5-10.1-8.4C22.8,6.4,18.3,12,18.3,17.7c0,4.7,1.9,5.6,1.4,6.6c-0.2,0.4-2.5,0.8-2.8,0.9c0,0,0,0,0,0 c-5.6,1.1-10.1,6.7-10.1,12.4c0,5.8,4.5,9.5,10.1,8.4c3.7-0.7,6.9-3.4,8.7-6.9c0,0,0,0,0,0c1.1-2.1,1.7-3.1,2.8-5.1 c1.1,1.6,1.7,2.4,2.8,4c0,0,0,0,0,0c1.8,2.7,5,4.2,8.7,3.4C45.5,40.3,50,34.8,50,29C50,23.3,45.5,19.5,39.9,20.6z'></path></svg>");
          width: 24px;
          height: 24px;
          position: absolute;
          left: -32px;
          top: 3px;
        }
      }
    }
    .img {
      flex: 1;
      img {
        width: 100%;
        height: auto;
        border-radius: 10px;
        box-shadow: var(--box-shadow);
      }
      @media ${QUERIES.sm} {
        padding: 0 80px;
      }
      @media ${QUERIES.md} {
        padding: 0;
      }
      @media ${QUERIES.lg} {
        flex: 2;
      }
    }
  `;

  return (
    <section css={characteristicsGoals}>
      <div className="characteristics">
        <h4>{characteristics.title}</h4>
        <ul>
          {characteristics.items.map(item => (
            <li key={item.id}>{item.text}</li>
          ))}
        </ul>
      </div>

      {goalsItems && (
        <div className="goals">
          <h4>{goalsTitle}</h4>
          <ul>
            {goalsItems.map(goalItem => (
              <li key={goalItem.id}>{goalItem.text}</li>
            ))}
          </ul>
        </div>
      )}

      {characteristics.image && (
        <div className="img">
          <GatsbyImage
            image={characteristics.image.img.childImageSharp.gatsbyImageData}
            alt={characteristics.image.alt}
            style={{ overflow: 'inherit' }}
          />
        </div>
      )}
    </section>
  );
};

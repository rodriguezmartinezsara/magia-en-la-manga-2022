import * as React from 'react';
import { useState } from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { graphql, useStaticQuery } from 'gatsby';
import { useCountUp } from '../hooks/useCountUp';
import VisibilitySensor from 'react-visibility-sensor';

export const Counter = ({ lang }) => {
  const getData = useStaticQuery(graphql`
    query GetCounterData {
      counterJson {
        es {
          number
          prefix
          suffix
          text
        }
        gl {
          number
          prefix
          suffix
          text
        }
      }
    }
  `);

  const counters = getData.counterJson[lang];

  const counterWrapper = css`
    background-color: var(--color-primary);
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    color: white;
    --space: 16px;
    padding: var(--space);
    gap: var(--space);
    text-align: center;
    justify-content: center;
    @media ${QUERIES.md} {
      grid-template-columns: repeat(4, minmax(100px, 280px));
      --space: 32px;
    }
  `;

  const [isCounterInViewport, setIsCounterInViewport] = useState(false);

  return (
    <VisibilitySensor
      active={!isCounterInViewport}
      partialVisibility
      onChange={isVisible => {
        if (isVisible) {
          setIsCounterInViewport(true);
        }
      }}
      delayedCall
    >
      <section css={counterWrapper}>
        {counters.map(counter => (
          <CountUp
            key={counter.text}
            end={counter.number}
            text={counter.text}
            prefix={counter?.prefix}
            suffix={counter?.suffix}
            startCounter={isCounterInViewport}
          />
        ))}
      </section>
    </VisibilitySensor>
  );
};

const CountUp = ({ end, text, prefix, suffix, startCounter }) => {
  const countUp = css`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-width: 0px;
    color: var(--color-grey-500);
    .number {
      font-size: clamp(2rem, 3.5vw + 1.5rem, 4rem);
    }
    .text {
      padding-bottom: 5px;
      overflow-wrap: break-word;
      width: 100%;
      @media ${QUERIES.md} {
        font-size: 1.5rem;
      }
    }
  `;

  const number = useCountUp(end, 3000, startCounter);

  return (
    <div css={countUp}>
      <span className="number">
        {prefix}
        {number}
        {suffix}
      </span>
      <span className="text">{text}</span>
    </div>
  );
};

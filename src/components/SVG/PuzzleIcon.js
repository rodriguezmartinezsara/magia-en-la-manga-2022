import * as React from 'react';
import { css } from '@emotion/react';

export const PuzzleIcon = () => {
  const puzzleIcon = css`
    fill: var(--color-primary);
    transition: fill 0.4s;
    @media (hover: hover) and (pointer: fine) {
      &:hover {
        fill: var(--color-primary-dark);
      }
    }
  `;

  return (
    <svg
      css={puzzleIcon}
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 283.5 283.5"
      version="1.1"
      viewBox="0 0 283.5 283.5"
      xmlSpace="preserve"
    >
      <path d="M116 146.9c-11.4 0-20.6 9.2-20.6 20.6 0 9 5.7 16.9 14.3 19.6.4.1.7.2 1.1.2 1.5 0 2.9-.9 3.3-2.4.6-1.8-.4-3.8-2.3-4.4-5.6-1.8-9.4-7-9.4-12.9 0-7.5 6.1-13.6 13.6-13.6 2.3 0 4.6.6 6.6 1.7 1.1.6 2.4.6 3.5-.1 1.1-.6 1.7-1.8 1.7-3v-26.5h18.8c-.4 1.5-.6 3.1-.6 4.7 0 9.2 6.2 17.3 15 19.8.3.1.6.1.9.1 1.5 0 2.9-1 3.4-2.6.5-1.9-.6-3.8-2.4-4.3-5.8-1.6-9.9-7-9.9-13.1 0-2.3.6-4.6 1.7-6.6.6-1.1.6-2.4-.1-3.5-.6-1.1-1.8-1.7-3-1.7h-23.9V99c1.5.4 3.1.6 4.7.6 11.4 0 20.6-9.2 20.6-20.6s-9.2-20.6-20.6-20.6c-1.6 0-3.2.2-4.7.6V40.2h56.6c12.3 0 22.2 10 22.2 22.2V119h-24.9c-1.2 0-2.4.7-3 1.7s-.6 2.4-.1 3.5c1.1 2 1.7 4.3 1.7 6.6v1c-.1 1.9 1.3 3.6 3.2 3.7 1.9.1 3.6-1.3 3.7-3.2 0-.5.1-1 .1-1.5 0-1.6-.2-3.2-.6-4.7H210c1.9 0 3.5-1.6 3.5-3.5V62.4c0-16.1-13.1-29.2-29.2-29.2H64.2C48.1 33.2 35 46.3 35 62.4v120.1c0 16.1 13.1 29.2 29.2 29.2h33.2c1.9 0 3.5-1.6 3.5-3.5s-1.6-3.5-3.5-3.5H64.2c-12.3 0-22.2-10-22.2-22.2V126h24.3c1.2 0 2.4-.7 3-1.7.6-1.1.6-2.4.1-3.5-1.1-2-1.7-4.3-1.7-6.6 0-7.5 6.1-13.6 13.6-13.6s13.6 6.1 13.6 13.6c0 2.3-.6 4.6-1.7 6.6-.6 1.1-.6 2.4.1 3.5.6 1.1 1.8 1.7 3 1.7h24.5v21.5c-1.6-.4-3.2-.6-4.8-.6zM101.2 119c.4-1.5.6-3.1.6-4.7 0-11.4-9.2-20.6-20.6-20.6s-20.6 9.2-20.6 20.6c0 1.6.2 3.2.6 4.7H41.9V62.4c0-12.3 10-22.2 22.2-22.2h56.6V64c0 1.2.7 2.4 1.7 3s2.4.6 3.5.1c2-1.1 4.3-1.7 6.6-1.7 7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6c-2.3 0-4.6-.6-6.6-1.7-1.1-.6-2.4-.6-3.5.1-1.1.6-1.7 1.8-1.7 3v25h-19.5zm139.3 67.1c-.1-.1-.2-.1-.3-.2l-28.8-19.5-11.8-30.9c-.3-.9-1-1.6-1.8-1.9-.8-.4-1.8-.4-2.7-.1l-25.6 9.8c-1.2.4-2 1.5-2.2 2.7s.2 2.5 1.2 3.3c1.7 1.5 3.1 3.4 3.9 5.5 2.7 7-.9 14.9-7.9 17.6-7 2.7-14.9-.9-17.6-7.9-.8-2.1-1.1-4.5-.8-6.7.2-1.2-.3-2.5-1.3-3.2-1-.8-2.3-1-3.4-.5L115 164c-.9.3-1.6 1-1.9 1.8-.4.8-.4 1.8-.1 2.7l8 20.9c-1.6.2-3.1.6-4.6 1.2-10.6 4-15.9 16-11.9 26.6s16 15.9 26.6 11.9c1.5-.6 2.9-1.3 4.2-2.2l8.3 21.8c.3.9 1 1.6 1.8 1.9.5.2.9.3 1.4.3.4 0 .8-.1 1.2-.2l35-13.4c1.9 4.6 5.6 8.1 9.8 8.7h.6c1.7 0 3.2-1.2 3.4-2.9.3-1.9-1-3.7-2.9-4-1.5-.3-3.4-2-4.4-4.3l10.4-4c1.1 2.8 2.8 5.3 4.9 7.6 5.3 5.6 12.4 8.2 16.7 8.2 1.9 0 3.5-1.6 3.5-3.5s-1.6-3.5-3.5-3.5c-2.5 0-7.7-1.9-11.6-6-3.3-3.5-4.9-7.6-4.7-12.3 0-1.1-.4-2.1-1.3-2.8-5.6-4.7-12.3-11.4-13.2-13.9-.6-1.8 0-4.2 1.6-5.7.9-.8 2.7-2 5.4-.9 2 .9 9.3 8 17 16.1l.1.1c2.6 2.8 5.2 5.6 7.7 8.4 1.3 1.4 3.5 1.6 4.9.3s1.6-3.5.3-4.9c-1.4-1.6-3.7-4.1-6.5-7.1 2-6.3 1.9-13.1-.5-19.3l-5.3-13.8 20.4 13.8c2.8 2.3 5.2 4.9 5.2 10.6l-.1 42.4c0 1.9 1.6 3.5 3.5 3.5s3.5-1.6 3.5-3.5l.1-42.4c.4-8.5-3.4-12.8-7.5-16.1zm-24.7 18.7c-5.9-6.1-12.2-12.1-15-13.3-4.5-1.9-9.4-1.1-12.9 2.3-3.6 3.4-5 8.7-3.4 13.1 1.7 4.9 10.4 12.7 14 15.9 0 .4.1.8.1 1.2l-14.2 5.4-35.4 13.5-8.9-23.3c-.4-1.2-1.5-2-2.7-2.2-1.2-.2-2.5.2-3.3 1.2-1.5 1.7-3.4 3.1-5.5 3.9-7 2.7-14.9-.9-17.6-7.9s.9-14.9 7.9-17.6c2.1-.8 4.5-1.1 6.7-.8 1.2.2 2.5-.3 3.2-1.3.8-1 1-2.3.5-3.4l-8.5-22.3 18.6-7.1c.2 1.6.6 3.1 1.2 4.6 2 5.1 5.8 9.2 10.8 11.5s10.6 2.4 15.8.4c10.6-4 16-16 11.9-26.6-.6-1.5-1.3-2.9-2.2-4.2l17.6-6.7 11 28.7 9.2 24.1c1.2 3.6 1.6 7.3 1.1 10.9z" />
    </svg>
  );
};

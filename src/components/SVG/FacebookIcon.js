import * as React from 'react';
import { css } from '@emotion/react';

export const FacebookIcon = () => {
  const facebookIcon = css`
    fill: var(--color-primary);
    transition: fill 0.4s;
    @media (hover: hover) and (pointer: fine) {
      &:hover {
        fill: var(--color-primary-dark);
      }
    }
  `;

  return (
    <svg
      css={facebookIcon}
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 283.5 283.5"
      version="1.1"
      viewBox="0 0 283.5 283.5"
      xmlSpace="preserve"
    >
      <path d="M141.7 7.1C49.3 7.1 7.1 53.6 7.1 146.1s42.2 130.3 134.6 130.3c92.5 0 134.6-37.8 134.6-130.3s-42.1-139-134.6-139zm43.6 78.6h-15.8c-12.4 0-14.8 5.9-14.8 14.5v19.1h29.5l-3.9 29.8h-25.7v76.6h-30.8v-76.6H98.1v-29.8h25.8v-22c0-25.5 15.6-39.4 38.4-39.4 10.9 0 20.3.8 23 1.2v26.6z" />
    </svg>
  );
};

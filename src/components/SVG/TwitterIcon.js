import * as React from 'react';
import { css } from '@emotion/react';

export const TwitterIcon = () => {
  const twitterIcon = css`
    fill: var(--color-primary);
    transition: fill 0.4s;
    @media (hover: hover) and (pointer: fine) {
      &:hover {
        fill: var(--color-primary-dark);
      }
    }
  `;

  return (
    <svg
      css={twitterIcon}
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 283.5 283.5"
      version="1.1"
      viewBox="0 0 283.5 283.5"
      xmlSpace="preserve"
    >
      <path d="M141.7 7.1C49.3 7.1 7.1 53.6 7.1 146.1s42.2 130.3 134.6 130.3c92.5 0 134.6-37.8 134.6-130.3.1-92.5-42.1-139-134.6-139zm58.6 104.6c.1 1.3.1 2.6.1 3.9 0 39.9-30.4 86-86 86-17.1 0-32.9-5-46.3-13.6 2.4.3 4.8.4 7.2.4 14.2 0 27.2-4.8 37.5-12.9-13.2-.2-24.4-9-28.2-21 1.8.4 3.7.5 5.7.5 2.8 0 5.4-.4 8-1.1C84.4 151.2 74 139 74 124.4v-.4c4.1 2.3 8.7 3.6 13.7 3.8a30.18 30.18 0 01-13.4-25.1c0-5.5 1.5-10.7 4.1-15.2 14.9 18.3 37.2 30.3 62.3 31.6-.5-2.2-.8-4.5-.8-6.9 0-16.7 13.5-30.2 30.2-30.2 8.7 0 16.5 3.7 22.1 9.5 6.9-1.4 13.3-3.9 19.2-7.3-2.3 7.1-7 13-13.3 16.7 6.1-.7 11.9-2.4 17.4-4.8-4.2 6-9.3 11.3-15.2 15.6z" />
    </svg>
  );
};

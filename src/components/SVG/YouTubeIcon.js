import * as React from 'react';
import { css } from '@emotion/react';

export const YouTubeIcon = () => {
  const youTubeIcon = css`
    fill: var(--color-primary);
    transition: fill 0.4s;
    @media (hover: hover) and (pointer: fine) {
      &:hover {
        fill: var(--color-primary-dark);
      }
    }
  `;

  return (
    <svg
      css={youTubeIcon}
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 283.5 283.5"
      version="1.1"
      viewBox="0 0 283.5 283.5"
      xmlSpace="preserve"
    >
      <path d="M141.7 7.1C49.3 7.1 7.1 53.6 7.1 146.1s42.2 130.3 134.6 130.3c92.5 0 134.6-37.8 134.6-130.3.1-92.5-42.1-139-134.6-139zm79.7 140.7c0 12.9-1.6 25.8-1.6 25.8s-1.6 11-6.3 15.8c-6.1 6.3-12.9 6.4-16 6.8-22.3 1.6-55.8 1.7-55.8 1.7s-41.4-.4-54.2-1.6c-3.5-.7-11.5-.5-17.6-6.8-4.8-4.8-6.3-15.8-6.3-15.8s-1.6-13-1.6-25.9v-12.1c0-12.9 1.6-25.8 1.6-25.8s1.6-11 6.3-15.8c6.1-6.3 12.9-6.4 16-6.8 22.3-1.6 55.8-1.6 55.8-1.6h.1s33.5 0 55.8 1.6c3.1.4 9.9.4 16 6.8 4.8 4.8 6.3 15.8 6.3 15.8s1.6 12.9 1.6 25.8v12.1zm-96.1-30.2l43.1 22.5-43.1 22.3v-44.8z" />
    </svg>
  );
};

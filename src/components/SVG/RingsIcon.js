import * as React from 'react';
import { css } from '@emotion/react';

export const RingsIcon = () => {
  const ringsIcon = css`
    fill: var(--color-primary);
    transition: fill 0.4s;
    @media (hover: hover) and (pointer: fine) {
      &:hover {
        fill: var(--color-primary-dark);
      }
    }
  `;

  return (
    <svg
      css={ringsIcon}
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 283.5 283.5"
      version="1.1"
      viewBox="0 0 283.5 283.5"
      xmlSpace="preserve"
    >
      <path d="M185.3 90.8c-15.9 0-30.9 4.9-43.6 14.2-7.6-5.5-16.2-9.6-25.5-12l20.1-21.8c.1-.1.1-.2.2-.2.1-.1.1-.2.2-.2.1-.1.1-.2.2-.3 0-.1.1-.2.1-.2.1-.1.1-.3.1-.4 0-.1 0-.1.1-.2 0-.2.1-.4.1-.7v-.2-.5c0-.1 0-.2-.1-.3 0-.1-.1-.2-.1-.4 0-.1-.1-.2-.1-.3-.1-.1-.1-.2-.2-.3-.1-.1-.1-.2-.2-.3 0-.1-.1-.1-.1-.2l-18-20.6c-.7-.8-1.6-1.2-2.6-1.2h-35c-1 0-2 .4-2.6 1.2L60 66.5c0 .1-.1.1-.1.2-.1.1-.1.2-.2.3-.1.1-.1.2-.2.3 0 .1-.1.2-.1.3 0 .1-.1.2-.1.4 0 .1-.1.2-.1.3v.7c0 .2 0 .4.1.7 0 .1 0 .1.1.2 0 .1.1.3.1.4 0 .1.1.2.1.2.1.1.1.2.2.3.1.1.1.2.2.2.1.1.1.2.2.2l20 21.8c-32.1 8.1-56 37.2-56 71.7 0 40.8 33.2 74 74 74 15.9 0 30.9-4.9 43.6-14.2 12.2 8.9 27.3 14.2 43.5 14.2 40.8 0 74-33.2 74-74s-33.2-73.9-74-73.9zm-89.5 0l-7.6-18.5h19.9l-7.6 18.5c-.8 0-1.6-.1-2.4-.1-.7.1-1.5.1-2.3.1zm12.1.6l7.8-19.1h10l-17.6 19.1h-.2zm18-26.1h-10.2l-5.1-13.6h3.4l11.9 13.6zm-22.7-13.5l5.1 13.6H88l5.1-13.6h10.1zm-20.9 0h3.4l-5.1 13.6H70.4l11.9-13.6zM70.7 72.3h10l7.8 19.1h-.2L70.7 72.3zm27.5 159.4c-36.9 0-67-30-67-67 0-32.9 23.9-60.4 55.3-65.9.8.1 1.6 0 2.3-.4 3.1-.4 6.2-.7 9.4-.7s6.3.2 9.4.7c.5.3 1.1.4 1.7.4.2 0 .4 0 .7-.1 31.4 5.6 55.2 33 55.2 65.9 0 14.4-4.7 28.4-13.2 39.9-2.1-1.7-4-3.6-5.8-5.6 7.2-10 10.9-21.8 10.9-34.2 0-13.6-4.6-26.1-12.4-36.1-.2-.3-.4-.5-.6-.7-3.2-4-6.9-7.5-10.9-10.5l-.1-.1c-.2-.2-.4-.3-.6-.4-9.7-7-21.5-11.1-34.3-11.1-32.5 0-59 26.5-59 59s26.5 59 59 59c11.5 0 22.6-3.3 32.2-9.5 1.8 2 3.7 3.9 5.8 5.7-11.3 7.6-24.3 11.7-38 11.7zm59.4-111c8.3-5.2 17.8-8 27.7-8 28.7 0 52 23.3 52 52s-23.3 52-52 52c-10.2 0-19.6-2.9-27.7-8 9.4-12.7 14.5-28.1 14.5-44 0-16.4-5.4-31.7-14.5-44zm-24.3 44c0-10.2 2.9-19.9 8.4-28.4 5.3 8.2 8.4 17.9 8.4 28.4 0 10.2-2.9 19.9-8.4 28.4-5.3-8.2-8.4-17.9-8.4-28.4zm-7.5-44c-9.4 12.7-14.5 28.1-14.5 44 0 16.5 5.4 31.7 14.5 44-8.3 5.2-17.8 8-27.7 8-28.7 0-52-23.3-52-52s23.3-52 52-52c10.2 0 19.7 3 27.7 8zm59.5 111c-36.9 0-67-30-67-67 0-14.4 4.7-28.4 13.2-39.9 2.1 1.7 4 3.6 5.8 5.6-7.2 10-10.9 21.8-10.9 34.2 0 13.6 4.6 26.1 12.4 36.1.2.3.4.5.6.7 3.2 4 6.9 7.5 10.9 10.5l.1.1c.2.2.4.3.6.4 9.7 7 21.5 11.1 34.3 11.1 32.5 0 59-26.5 59-59s-26.5-59-59-59c-11.5 0-22.6 3.3-32.2 9.5-1.8-2-3.7-3.9-5.8-5.7 11.2-7.7 24.2-11.8 37.9-11.8 36.9 0 67 30 67 67s-30 67.2-66.9 67.2z" />
    </svg>
  );
};

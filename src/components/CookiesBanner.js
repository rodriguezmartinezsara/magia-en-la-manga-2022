import { CookieConsent } from 'react-cookie-consent';
import { Link } from 'gatsby';
import React from 'react';
import { css } from '@emotion/react';

export function CookiesBanner({ setGpdrAccepted }) {
  const cookiesBanner = css`
    a {
      text-decoration: none;
      color: var(--color-grey-300);
      transition: color 0.4s;
      &:hover {
        color: var(--color-primary);
      }
    }
    #rcc-confirm-button {
      transition: all 0.4s !important;
      &:hover {
        background-color: transparent !important;
        color: var(--color-primary) !important;
      }
    }
  `;

  return (
    <div css={cookiesBanner}>
      <CookieConsent
        // debug={true}
        enableDeclineButton
        // flipButtons
        // disableStyles={true}
        location="bottom"
        buttonText="Aceptar"
        declineButtonText="Rechazar"
        cookieName="accepted-gdpr"
        style={{
          background: '#2A2A27',
          color: 'white',
          borderTop: '1px solid lightgray',
        }}
        buttonStyle={{
          background: '#EFD384',
          borderRadius: '5px',
          color: '#2A2A27',
          border: '1px solid #EFD384',
          paddingLeft: '15px',
          paddingRight: '15px',
          marginRight: '30px',
          textTransform: 'uppercase',
          fontSize: '15px',
        }}
        declineButtonStyle={{
          background: 'none',
          borderRadius: '5px',
          color: '#EFD384',
          border: '1px solid #EFD384',
          textTransform: 'uppercase',
          fontSize: '13px',
        }}
        // buttonClasses="inline-flex justify-center mt-4 color-red-500 py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indagres-green focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-400"
        onAccept={() => {
          setGpdrAccepted(true);
        }}
        setDeclineCookie={false}
      >
        Utilizamos cookies para mejorar la experiencia de usuario.{' '}
        <Link to="/cookies">Más información</Link>
        {/*<span style={{ fontSize: '10px' }}>This bit of text is smaller :O</span>*/}
      </CookieConsent>
    </div>
  );
}

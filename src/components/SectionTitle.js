import * as React from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const SectionTitle = ({ title, subtitle }) => {
  const sectionTitle = css`
    text-align: center;
    margin: 0 auto;
    padding: 40px 24px 32px;
    max-width: 750px;
    @media ${QUERIES.lg} {
      max-width: 1050px;
      padding: 40px 32px 32px;
    }
    h2 {
      position: relative;
      padding-bottom: 16px;
      font-size: var(--fs-800);
      font-weight: 600;
      &:after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 0;
        right: 50%;
        transform: translateX(50%);
      }
    }
    p {
      color: var(--color-grey-300);
      padding-top: 16px;
    }
  `;

  return (
    <div css={sectionTitle} className="section-title">
      <h2>{title}</h2>
      {subtitle && <p>{parse(subtitle)}</p>}
    </div>
  );
};

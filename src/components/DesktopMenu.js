import * as React from 'react';
import { Link } from 'gatsby';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const DesktopMenu = ({ menuLinks, lang, translationSlug }) => {
  const desktopMenu = css`
    display: none;
    @media ${QUERIES.lg} {
      display: flex;
      gap: 24px;
      .menu {
        display: flex;
        font-size: 1.15rem;
        font-weight: 700;
        gap: 8px;
        list-style: none;
        padding: 0;
        align-items: baseline;
        li {
          position: relative;
          &.dropdown {
            padding-right: 16px;
            &:hover::after {
              content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='%23F0D283' stroke-width='2'  stroke-linecap='round'><polyline points='6 9 12 15 18 9'/></svg>");
            }
          }
          &.dropdown::after {
            //FIXME arreglar esto para que el color venga de una variable
            content: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='white' stroke-width='2' stroke-linecap='round'><polyline points='6 9 12 15 18 9'/></svg>");
            width: 22px;
            height: 22px;
            position: absolute;
            right: 0px;
            top: 5px;
          }
          a {
            padding: 8px;
            text-decoration: none;
            color: var(--color-white);
            transition: color 0.5s ease;
            &.active {
              color: var(--color-primary);
            }
            @media (hover: hover) and (pointer: fine) {
              &:hover {
                color: var(--color-primary);
              }
            }
          }
          &.translation {
            font-size: 0.85rem;
            a {
              margin-left: 16px;
              color: var(--color-grey-200);
              @media (hover: hover) and (pointer: fine) {
                &:hover {
                  color: var(--color-primary);
                }
              }
            }
          }
          &:hover > .submenu,
          .submenu:hover {
            visibility: visible;
            opacity: 1;
            display: block;
            min-width: 200px;
          }
          .submenu {
            visibility: hidden;
            list-style: none;
            opacity: 0;
            position: absolute;
            padding: 0;
            left: -10px;
            display: none;
            li {
              width: 100%;
              background: var(--color-grey-600);
              &:hover {
                background: var(--color-grey-500);
              }
              &:not(:last-of-type) {
                border-bottom: 1px solid var(--color-grey-300);
              }
              a {
                display: block;
                padding: 8px 16px;
              }
            }
          }
        }
      }
    }
  `;

  return (
    <nav css={desktopMenu}>
      <ul className="menu">
        {menuLinks.map(link => (
          <li key={link.name} className={link.subMenu ? 'dropdown' : ''}>
            <Link to={link.link} activeClassName="active">
              {link.name}
            </Link>
            {link.subMenu && (
              <ul className="submenu" aria-label="submenu">
                {link.subMenu.map(subLink => (
                  <li key={subLink.name}>
                    <Link to={subLink.link} activeClassName="active">
                      {subLink.name}
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
        {translationSlug && (
          <li className="translation">
            <Link to={translationSlug}>{lang === 'es' ? 'GL' : 'ES'}</Link>
          </li>
        )}
      </ul>
    </nav>
  );
};

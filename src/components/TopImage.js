import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';

export const TopImage = ({ title, image }) => {
  const topImage = css`
    min-height: 200px;
    background: linear-gradient(
        0deg,
        rgba(60, 60, 60, 0.6),
        rgba(60, 60, 60, 0.6)
      ),
      url('/magia-en-la-manga-02.webp');
    background-position: center;
    background-size: cover;
    display: flex;
    @media ${QUERIES.lg} {
      min-height: 250px;
    }
    .titleWrapper {
      width: 100%;
      max-width: 1650px;
      margin: 0 auto;
      align-self: end;
    }
    h1 {
      color: var(--color-white);
      font-size: var(--fs-900);
      font-weight: 500;
      padding: 72px 16px 12px;
      @media ${QUERIES.md} {
        padding: 72px 32px 20px;
      }
      @media ${QUERIES.lg} {
        width: 55%;
      }
    }
  `;

  return (
    <section css={topImage}>
      <div className="titleWrapper">
        <h1>{title}</h1>
      </div>
    </section>
  );
};

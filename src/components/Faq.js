import * as React from 'react';
import { useState } from 'react';
import parse from 'html-react-parser';
import { css } from '@emotion/react';

export const Faq = ({ questions }) => {
  const [openFaqId, setOpenFaqId] = useState('');
  const toggleActiveFaq = id => e => {
    e.preventDefault();
    setOpenFaqId(id !== openFaqId ? id : '');
  };

  const faq = css`
    padding: 0 16px 40px;
    max-width: 1250px;
    margin: 0 auto;
    details {
      border-bottom: 2px solid var(--color-grey-100);
      border-right: 2px solid var(--color-grey-100);
      border-left: 2px solid var(--color-grey-100);
      overflow-wrap: break-word;
      &:first-of-type {
        border-top: 2px solid var(--color-grey-100);
      }
      &[open] {
        summary {
          color: var(--color-primary-dark);
          &::after {
            transform: translateY(-56%) rotate(45deg);
          }
        }
      }
    }
    summary {
      padding: 16px 40px 16px 24px;
      text-transform: uppercase;
      font-weight: 700;
      list-style: none;
      cursor: pointer;
      position: relative;
      transition: color 200ms;
      &::after {
        content: '+';
        position: absolute;
        right: 20px;
        top: 50%;
        transform: translateY(-56%);
        font-size: 25px;
        transition: transform 200ms;
      }
    }
    div {
      padding: 32px 40px;
      border-top: 2px solid var(--color-grey-100);
    }
    p:not(:first-of-type) {
      padding-top: 16px;
    }
  `;

  return (
    <div css={faq}>
      {questions.map(question => (
        <details key={question.id} open={openFaqId === question.id}>
          <summary onClick={toggleActiveFaq(question.id)}>
            {question.question}
          </summary>
          <div>{parse(question.answer)}</div>
        </details>
      ))}
    </div>
  );
};

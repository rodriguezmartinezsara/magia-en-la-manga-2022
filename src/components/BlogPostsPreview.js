import * as React from 'react';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { graphql, Link, useStaticQuery } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';

export const BlogPostsPreview = ({
  searchInput = '',
  setSearchInput,
  category = '',
  setCategory,
}) => {
  const getData = useStaticQuery(graphql`
    query GetBlogPostsPreviewData {
      allPostsJson(sort: { fields: date, order: DESC }) {
        nodes {
          id
          slug
          title
          synopsis
          category
          mainImage {
            alt
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
          content {
            paragraph
            columns {
              paragraphs
            }
          }
        }
      }
    }
  `);

  const normalizeText = text => {
    return text
      .normalize('NFD')
      .replace(/\p{Diacritic}/gu, '')
      .toLowerCase()
      .trim();
  };

  const postsPreview = getData.allPostsJson.nodes.filter(node => {
    if (category) {
      return node.category === category;
    } else {
      return (
        normalizeText(node.title).includes(normalizeText(searchInput)) ||
        normalizeText(node.synopsis).includes(normalizeText(searchInput)) ||
        node.content
          .filter(c => c.paragraph)
          .some(e =>
            normalizeText(e.paragraph).includes(normalizeText(searchInput))
          ) ||
        node.content
          .filter(c => c.columns?.paragraphs)
          .map(c => c.columns.paragraphs)
          .flat()
          .some(p => normalizeText(p).includes(normalizeText(searchInput)))
      );
    }
  });

  const blogPostsPreview = css`
    display: grid;
    gap: 32px;
    grid-template-columns: repeat(auto-fill, minmax(min(280px, 100%), 1fr));
    margin: 0 auto;
    @media ${QUERIES.md} {
      gap: 48px;
    }
    & > div {
      background-color: white;
      --space: 24px;
      padding: var(--space);
      padding-top: 0;
      box-shadow: var(--box-shadow);
      border-radius: 0 0 5px 5px;
      @media ${QUERIES.md} {
        --space: 32px;
      }
      .stretched-out {
        margin-right: calc(var(--space) * -1);
        margin-left: calc(var(--space) * -1);
        img {
          object-fit: cover;
          width: 100%;
          aspect-ratio: 3 / 1;
          @media ${QUERIES.md} {
            aspect-ratio: 2 / 1;
          }
        }
      }
      a {
        text-decoration: none;
        color: inherit;
        transition: color 0.4s;
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
      h2 {
        font-size: 1.5rem;
        padding-top: 24px;
        transition: color 0.4s;
        @media ${QUERIES.md} {
          font-size: 2rem;
        }
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
      p {
        padding-top: 24px;
      }
      .synopsis {
        display: none;
        @media ${QUERIES.md} {
          display: inherit;
        }
      }
      .category {
        color: var(--color-grey-300);
        position: relative;
        &::after {
          content: '';
          background-color: var(--color-primary);
          width: 40px;
          height: 2px;
          position: absolute;
          bottom: -10px;
          left: 0;
        }
      }
      .category,
      .read-more {
        text-transform: uppercase;
        font-weight: bold;
        font-size: 0.9rem;
      }
      .read-more {
        position: relative;
        transition: color 0.4s;
        &::after {
          content: '→';
          position: absolute;
          padding-left: 8px;
        }
        @media (hover: hover) and (pointer: fine) {
          &:hover {
            color: var(--color-primary-dark);
          }
        }
      }
    }
  `;

  return (
    <div css={blogPostsPreview}>
      {postsPreview.length > 0 ? (
        postsPreview.map(postPreview => (
          <div key={postPreview.id}>
            <Link
              to={`${postPreview.slug}`}
              onClick={() => {
                setCategory('');
                setSearchInput('');
              }}
            >
              <div className="stretched-out">
                <GatsbyImage
                  image={
                    postPreview.mainImage.img.childImageSharp.gatsbyImageData
                  }
                  alt={postPreview.mainImage.alt}
                />
              </div>
            </Link>
            <p className="category">{postPreview.category}</p>
            <Link
              to={`${postPreview.slug}`}
              onClick={() => {
                setCategory('');
                setSearchInput('');
              }}
            >
              <h2>{postPreview.title}</h2>
            </Link>
            <p className="synopsis">{postPreview.synopsis}</p>
            <Link
              to={`${postPreview.slug}`}
              onClick={() => {
                setCategory('');
                setSearchInput('');
              }}
            >
              <p className="read-more">Leer más</p>
            </Link>
          </div>
        ))
      ) : (
        <p>No se han encontrado resultados. ¡Prueba otra cosa!</p>
      )}
    </div>
  );
};

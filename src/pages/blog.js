import * as React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { TopImageBlog } from '../components/TopImageBlog';
import { BlogLayout } from '../components/BlogLayout';

const BlogPage = () => {
  return (
    <Layout lang="es">
      <Seo
        title="Blog"
        description="¡Lee nuestras historias en el blog de Magia en la Manga!"
        lang="es"
        bodyClasses="background-blog"
      />
      <TopImageBlog title="Blog" />
      <BlogLayout />
    </Layout>
  );
};

export default BlogPage;

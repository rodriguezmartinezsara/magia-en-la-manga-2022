import * as React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { PublicEventsContent } from '../components/pages/PublicEventsContent';

const EventosPublicosEsPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEventosPublicosEsData {
      publicEventsJson {
        es {
          title
          metaDescription
          showsInfo {
            title
            subtitle
          }
          workshopsInfo {
            title
            subtitle
          }
          contact {
            title
            description
          }
        }
        gl {
          slug
        }
      }
      shows: allShowsJson(filter: { tag: { eq: "public" } }) {
        nodes {
          es {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
      workshops: allShowsJson(filter: { tag: { eq: "workshop" } }) {
        nodes {
          es {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
    }
  `);

  const publicEvents = getData.publicEventsJson.es;
  const shows = getData.shows.nodes.map(e => e.es);
  const workshops = getData.workshops.nodes.map(e => e.es);
  const translationSlug = getData.publicEventsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={publicEvents.title}
        description={publicEvents.metaDescription}
        lang="es"
      />
      <PublicEventsContent
        publicEvents={publicEvents}
        shows={shows}
        workshops={workshops}
        lang="es"
      />
    </Layout>
  );
};

export default EventosPublicosEsPage;

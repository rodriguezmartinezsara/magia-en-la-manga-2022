import * as React from 'react';
import Layout from '../components/Layout';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import { TopImage } from '../components/TopImage';
import Seo from '../components/Seo';

const CookiesPage = () => {
  const cookies = css`
    padding: 36px 24px;
    max-width: 1250px;
    margin: 0 auto;
    @media ${QUERIES.md} {
      padding: 36px 48px;
    }
    h2 {
      padding: 24px 0 16px;
      position: relative;
      &::after {
        content: '';
        background-color: var(--color-primary);
        width: 60px;
        height: 2px;
        position: absolute;
        bottom: 4px;
        left: 32px;
      }
    }
    p {
      padding: 8px 0;
    }
  `;

  return (
    <Layout lang="es">
      <Seo title="Política de Cookies" lang="es" />
      <TopImage title="Política de Cookies" />
      <div css={cookies}>
        <h2>Concepto de Cookies</h2>
        <p>
          Las cookies constituyen una herramienta empleada por los servidores
          Web para almacenar y recuperar información acerca de sus visitantes o
          usuarios.
        </p>
        <p>
          No son más que unos ficheros de texto que algunos servidores instalan
          durante la navegación y que permiten conocer información como por
          ejemplo el lugar desde el que se accede, el tiempo de conexión, el
          dispositivo desde el que accede (fijo o móvil), el sistema operativo y
          navegador utilizados, las páginas más visitadas, elnú mero de clicks
          realizados y datos respecto al comportamiento del usuario en Internet.
        </p>
        <p>
          No proporcionan referencias que permitan deducir el nombre y apellidos
          del usuario y no pueden leer datos de su disco duro ni incluir virus
          en sus equipos.
        </p>
        <p>
          Asimismo, no pueden leer las cookies implantadas en el disco duro del
          usuario desde otros servidores.
        </p>
        <h2>¿Por qué son importantes?</h2>
        <p>
          El Sitio Web es accesible sin necesidad de que las cookies estén
          activadas, si bien, su desactivación puede impedir su funcionamiento
          correcto.
        </p>
        <p>
          Desde un punto de vista técnico las cookies permiten que los sitios
          web funcionen de forma más ágil y adaptada a las preferencias de los
          usuarios, como por ejemplo almacenando el idioma, la moneda del país o
          detectando el dispositivo de acceso.
        </p>
        <p>
          Establecen niveles de protección y seguridad que impiden o dificultan
          ciberataques contra el Sitio Web o sus usuarios.
        </p>
        <p>
          Permiten que los gestores de los medios puedan conocer datos
          estadísticos recopilados para mejorar la calidad y experiencia de sus
          servicios.
        </p>
        <p>
          Sirven para optimizar la publicidad que mostramos a los usuarios,
          ofreciendo la que más se ajusta a sus intereses.
        </p>

        <h2>Autorización para el uso de cookies</h2>
        <p>
          En el Sitio Web aparece un Aviso de Cookies que el usuario o visitante
          puede aceptar, consintiendo expresamente al uso de las cookies según
          se indica a continuación.
        </p>
        <p>
          El usuario puede configurar su navegador para rechazar por defecto
          todas las cookies o para recibir un aviso en pantalla de la recepción
          de cada cookie y decidir sobre su instalación o no en su disco duro.
        </p>

        <h2>Configuración del navegador</h2>
        <p>
          magiaenlamanga.com recuerda a sus usuarios que el uso de cookies podrá
          estar sujeto a su aceptación durante la instalación o actualización
          del navegador utilizado por estos.
        </p>
        <p>
          Esta aceptación podrá ser revocada mediante las opciones de
          configuración de contenidos y privacidad disponibles en el mismo. El
          Titular recomienda a sus usuarios que consulten la ayuda de su
          navegador o accedan a las páginas web de ayuda de los principales
          navegadores: Firefox, Internet Explorer, Safari, Chrome.
        </p>

        <h2>Tipos de Cookies que se utilizan en la Web</h2>
        <p>
          El usuario que navega por la Web puede encontrar cookies insertadas
          directamente por el titular, o bien cookies insertadas por entidades
          distintas al titular, según lo detallado en los siguientes apartados:
        </p>
        <ul>
          <li>
            <p>
              <strong>Cookies de sesión</strong>, expiran cuando el usuario
              abandona la página o cierra el navegador, es decir, están activas
              mientras dura la visita al Sitio Web y por tanto son borradas de
              nuestro ordenador al abandonarlo.
            </p>
          </li>
          <li>
            <p>
              <strong>Cookies permanentes</strong>, expiran cuando se cumple el
              objetivo para el que sirven o bien cuando se borran manualmente,
              tienen fecha de borrado y se utilizan normalmente en el proceso de
              compra online, personalizaciones o en el registro, para no tener
              que introducir una contraseña constantemente.
            </p>
            <p>
              Según la entidad que gestiona el equipo y trata los datos que se
              obtengan o el dominio desde donde se envían las cookies, podemos
              distinguir entre cookies propias y de terceros.
            </p>
          </li>
          <li>
            <p>
              <strong>Cookies propias</strong> son aquellas cookies que son
              enviadas al ordenador del usuario y son gestionadas exclusivamente
              por nosotros para el mejor funcionamiento del Sitio Web. La
              información que recabamos se emplea para mejorar la calidad de
              nuestro servicio y su experiencia como usuario.
            </p>
          </li>
          <li>
            <p>
              <strong>Cookies de terceros</strong>, cuando el usuario interactúa
              con el contenido de nuestro Sitio Web también pueden establecerse
              cookies de terceros (por ejemplo, al pulsar botones de redes
              sociales o visionar vídeos alojados en otro sitio web), que son
              aquellas establecidas por un dominio diferente de nuestro Sitio
              Web. No podemos acceder a los datos almacenados en las cookies de
              otros sitios web cuando navegue en los citados sitios web.
            </p>
          </li>
        </ul>
        <p>
          Navegar por este portal web supone que se puedan instalar los
          siguientes tipos de Cookies:
        </p>
        <ul>
          <li>
            <p>
              <strong>De rendimiento</strong>, se utilizan para mejorar su
              experiencia de navegación y optimizar el funcionamiento de
              nuestros sitios web. Almacenan configuraciones de servicios para
              que no tenga que reconfigurarlos cada vez que nos visita.
            </p>
          </li>
          <li>
            <p>
              <strong>De geolocalización</strong>, se utilizan para almacenar
              datos de geolocalización del ordenador o dispositivos para
              ofrecerle contenidos y servicios más adecuados.
            </p>
          </li>
          <li>
            <p>
              <strong>De registro</strong>, se crean al registrarte o cuando
              inicia sesión en uno de nuestros portales web.
            </p>
          </li>
          <li>
            <p>
              <strong>Analíticas</strong>, recopilan información de su
              experiencia de navegación en nuestros portales web de forma
              totalmente anónima.
            </p>
          </li>
          <li>
            <p>
              <strong>De publicidad</strong>, recogen información sobre los
              anuncios que pudiesen ser mostrados a cada usuario anónimo en los
              portales web.
            </p>
          </li>
        </ul>
      </div>
    </Layout>
  );
};

export default CookiesPage;

import * as React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { EducationalEventsContent } from '../components/pages/EducationalEventsContent';

const MagiaEducativaPage = () => {
  const getData = useStaticQuery(graphql`
    query GetMagiaEducativaData {
      educationalEventsJson {
        es {
          title
          metaDescription
          subtitle
          mainImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          mainParagraph
          showsInfo {
            title
            subtitle
          }
          workshopsInfo {
            title
            subtitle
          }
          workshops {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
          }
          faq {
            title
            questions {
              id
              question
              answer
            }
          }
          contact {
            title
            description
          }
        }
        gl {
          slug
        }
      }
      allShowsJson(filter: { tag: { eq: "edu" } }) {
        nodes {
          es {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
    }
  `);

  const educationalEvents = getData.educationalEventsJson.es;
  const shows = getData.allShowsJson.nodes.map(e => e.es);
  const translationSlug = getData.educationalEventsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={educationalEvents.title}
        description={educationalEvents.metaDescription}
        lang="es"
      />
      <EducationalEventsContent
        educationalEvents={educationalEvents}
        shows={shows}
        lang="es"
      />
    </Layout>
  );
};

export default MagiaEducativaPage;

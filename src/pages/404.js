import * as React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { TopImage } from '../components/TopImage';
import { css } from '@emotion/react';
import { QUERIES } from '../utils/constants';
import Sparkles from '../components/Sparkles';
import { TopHatAnimation } from '../components/TopHatAnimation';

const NotFoundPage = () => {
  const wrapper = css`
    max-width: 650px;
    margin: 0 auto;
    padding: 48px 32px 24px;
    @media ${QUERIES.md} {
      display: flex;
      max-width: 1400px;
      gap: 36px;
      align-items: center;
      padding: 48px;
    }
    @media ${QUERIES.lg} {
      gap: 64px;
      padding: 48px 64px;
    }
    @keyframes background-pan {
      from {
        background-position: 0 center;
      }
      to {
        background-position: -200% center;
      }
    }
    .magic-text {
      animation: background-pan 3s linear infinite;
      background: linear-gradient(
        to right,
        var(--color-primary-dark),
        var(--color-primary-medium),
        var(--color-primary),
        var(--color-primary-dark)
      );
      background-size: 200%;
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
    .text {
      h2 {
        font-size: var(--fs-500);
        font-weight: 400;
        word-break: break-word;
      }
    }
    .animation {
      max-width: 500px;
      margin: 0 auto;
    }
    @media ${QUERIES.md} {
      .text,
      .animation {
        flex: 1;
      }
      .animation {
        max-width: revert;
      }
    }
    @media ${QUERIES.lg} {
      .text {
        h2 {
          padding: 24px;
        }
      }
    }
    h1 {
      font-size: clamp(90px, 20vw, 320px);
      font-weight: 800;
      color: var(--primario-hover);
    }
  `;

  return (
    <Layout>
      <Seo title="404: Página no encontrada" />
      <TopImage title="Página no encontrada" />
      <div css={wrapper}>
        <div className="text">
          <h2>
            El contenido ha desaparecido{' '}
            <Sparkles>
              <span className="magic-text">mágicamente...</span>
            </Sparkles>{' '}
            o lo hemos teletransportado
          </h2>
        </div>
        <div className="animation">
          <TopHatAnimation />
        </div>
      </div>
    </Layout>
  );
};

export default NotFoundPage;

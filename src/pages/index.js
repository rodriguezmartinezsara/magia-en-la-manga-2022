import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { IndexContent } from '../components/pages/IndexContent';

const IndexEsPage = () => {
  const getData = useStaticQuery(graphql`
    query GetIndexEsData {
      infoIndexJson {
        es {
          metaDescription
          mainSection {
            title
            description
            link
            linkText
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          services {
            title
            typeOfService {
              link
              icon
              title
              description
            }
          }
          gallery {
            title
            images {
              id
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
              width
              height
            }
          }
          clients {
            title
            logos {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          contact {
            title
            description
          }
        }
        gl {
          slug
        }
      }
    }
  `);

  const infoIndex = getData.infoIndexJson.es;
  const translationSlug = getData.infoIndexJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo title="Inicio" description={infoIndex.metaDescription} lang="es" />
      <IndexContent infoIndex={infoIndex} lang={'es'} />
    </Layout>
  );
};

export default IndexEsPage;

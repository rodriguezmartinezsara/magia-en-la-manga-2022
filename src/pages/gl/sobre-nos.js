import * as React from 'react';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { AboutUsContent } from '../../components/pages/AboutUsContent';

const SobreNosPage = () => {
  const getData = useStaticQuery(graphql`
    query GetSobreNosData {
      aboutUsJson {
        gl {
          title
          metaDescription
          sections {
            title
            subtitle
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          team {
            title
            teamMembers {
              title
              image {
                img {
                  childImageSharp {
                    gatsbyImageData(placeholder: BLURRED)
                  }
                }
                alt
              }
              description
              socialNetworks {
                icon
                link
              }
            }
          }
          contact {
            title
            description
          }
        }
        es {
          slug
        }
      }
    }
  `);

  const aboutUs = getData.aboutUsJson.gl;
  const translationSlug = getData.aboutUsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={aboutUs.title}
        description={aboutUs.metaDescription}
        lang="gl"
      />
      <AboutUsContent aboutUs={aboutUs} lang="gl" />
    </Layout>
  );
};

export default SobreNosPage;

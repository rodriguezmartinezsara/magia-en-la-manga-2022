import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { PrivateEventsContent } from '../../components/pages/PrivateEventsContent';

const ParticularesGlPage = () => {
  const getData = useStaticQuery(graphql`
    query GetParticularesGlData {
      privateEventsJson {
        gl {
          title
          metaDescription
          subtitle
          description
          contact {
            title
            description
          }
          typeOfEvent {
            title
            icon
            link
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            characteristics {
              id
              item
            }
          }
          servicesHeader: typeOfEvent {
            title
            icon
            link
          }
        }
        es {
          slug
        }
      }
    }
  `);

  const privateEvents = getData.privateEventsJson.gl;
  const translationSlug = getData.privateEventsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={privateEvents.title}
        description={privateEvents.metaDescription}
        lang="gl"
      />
      <PrivateEventsContent privateEvents={privateEvents} />
    </Layout>
  );
};

export default ParticularesGlPage;

import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { IndexContent } from '../../components/pages/IndexContent';

const IndexGlPage = () => {
  const getData = useStaticQuery(graphql`
    query GetIndexGlData {
      infoIndexJson {
        gl {
          metaDescription
          mainSection {
            title
            description
            link
            linkText
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          services {
            title
            typeOfService {
              link
              icon
              title
              description
            }
          }
          gallery {
            title
            images {
              id
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
              width
              height
            }
          }
          clients {
            title
            logos {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          contact {
            title
            description
          }
        }
        es {
          slug
        }
      }
    }
  `);

  const infoIndex = getData.infoIndexJson.gl;
  const translationSlug = getData.infoIndexJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo title="Inicio" description={infoIndex.metaDescription} lang="gl" />
      <IndexContent infoIndex={infoIndex} lang={'gl'} />
    </Layout>
  );
};

export default IndexGlPage;

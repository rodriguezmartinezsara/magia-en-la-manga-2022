import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { CorporateEventsContent } from '../../components/pages/CorporateEventsContent';

const EmpresasGlPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEmpresasGlData {
      corporateEventsJson {
        gl {
          title
          metaDescription
          subtitle
          description
          contact {
            title
            description
          }
          typeOfEvent {
            title
            icon
            link
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            characteristics {
              id
              item
            }
          }
          services: typeOfEvent {
            title
            icon
            link
          }
        }
        es {
          slug
        }
      }
    }
  `);

  const corporateEvents = getData.corporateEventsJson.gl;
  const translationSlug = getData.corporateEventsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={corporateEvents.title}
        description={corporateEvents.metaDescription}
        lang="gl"
      />
      <CorporateEventsContent corporateEvents={corporateEvents} />
    </Layout>
  );
};

export default EmpresasGlPage;

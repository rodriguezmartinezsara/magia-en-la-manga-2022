import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { MagicSchoolContent } from '../../components/pages/MagicSchoolContent';

const EscolaDeMaxiaGlPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEscolaData {
      magicSchoolJson {
        gl {
          title
          metaDescription
          subtitle
          mainParagraph
          secondParagraph
          mainImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          secondImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          signUp {
            items {
              id
              text
            }
            title
            subtitle
            link
          }
          space {
            title
            mainParagraph
            characteristics {
              items {
                id
                text
              }
            }
            video {
              url
              title
            }
          }
          faq {
            title
            questions {
              id
              question
              answer
            }
          }
          gallery {
            title
            images {
              id
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
              width
              height
            }
          }
          contact {
            title
            description
          }
        }
        es {
          slug
        }
      }
    }
  `);

  const magicSchool = getData.magicSchoolJson.gl;
  const translationSlug = getData.magicSchoolJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={magicSchool.title}
        description={magicSchool.metaDescription}
        lang="gl"
      />
      <MagicSchoolContent magicSchool={magicSchool} lang={'gl'} />
    </Layout>
  );
};

export default EscolaDeMaxiaGlPage;

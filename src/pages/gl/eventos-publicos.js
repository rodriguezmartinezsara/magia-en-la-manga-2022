import * as React from 'react';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { PublicEventsContent } from '../../components/pages/PublicEventsContent';

const EventosPublicosGlPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEventosPublicosGlData {
      publicEventsJson {
        gl {
          title
          metaDescription
          showsInfo {
            title
            subtitle
          }
          workshopsInfo {
            title
            subtitle
          }
          contact {
            title
            description
          }
        }
        es {
          slug
        }
      }
      shows: allShowsJson(filter: { tag: { eq: "public" } }) {
        nodes {
          gl {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
      workshops: allShowsJson(filter: { tag: { eq: "workshop" } }) {
        nodes {
          gl {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
    }
  `);

  const publicEvents = getData.publicEventsJson.gl;
  const shows = getData.shows.nodes.map(e => e.gl);
  const workshops = getData.workshops.nodes.map(e => e.gl);
  const translationSlug = getData.publicEventsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={publicEvents.title}
        description={publicEvents.metaDescription}
        lang="gl"
      />
      <PublicEventsContent
        publicEvents={publicEvents}
        shows={shows}
        workshops={workshops}
        lang="gl"
      />
    </Layout>
  );
};

export default EventosPublicosGlPage;

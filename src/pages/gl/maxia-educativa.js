import * as React from 'react';
import Layout from '../../components/Layout';
import Seo from '../../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { EducationalEventsContent } from '../../components/pages/EducationalEventsContent';

const MaxiaEducativaPage = () => {
  const getData = useStaticQuery(graphql`
    query GetMaxiaEducativaData {
      educationalEventsJson {
        gl {
          title
          metaDescription
          subtitle
          mainImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          mainParagraph
          showsInfo {
            title
            subtitle
          }
          workshopsInfo {
            title
            subtitle
          }
          workshops {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
          }
          faq {
            title
            questions {
              id
              question
              answer
            }
          }
          contact {
            title
            description
          }
        }
        es {
          slug
        }
      }
      allShowsJson(filter: { tag: { eq: "edu" } }) {
        nodes {
          gl {
            title
            smallImage {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            synopsis
            slug
          }
        }
      }
    }
  `);

  const educationalEvents = getData.educationalEventsJson.gl;
  const shows = getData.allShowsJson.nodes.map(e => e.gl);
  const translationSlug = getData.educationalEventsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo
        title={educationalEvents.title}
        description={educationalEvents.metaDescription}
        lang="gl"
      />
      <EducationalEventsContent
        educationalEvents={educationalEvents}
        shows={shows}
        lang="gl"
      />
    </Layout>
  );
};

export default MaxiaEducativaPage;

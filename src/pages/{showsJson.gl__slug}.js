import * as React from 'react';
import Seo from '../components/Seo';
import Layout from '../components/Layout';
import { graphql } from 'gatsby';
import { ShowContent } from '../components/pages/ShowContent';

const ShowGl = props => {
  const show = props.data.showsJson.gl;
  const contact = props.data.educationalEventsJson.gl.contact;
  const translationSlug = props.data.showsJson.es.slug;

  return (
    <Layout lang="gl" translationSlug={translationSlug}>
      <Seo title={show.title} description={show.metaDescription} lang="gl" />
      <ShowContent show={show} contact={contact} />
    </Layout>
  );
};

export default ShowGl;

export const query = graphql`
  query ShowGlById($id: String) {
    showsJson(id: { eq: $id }) {
      id
      gl {
        title
        metaDescription
        subtitle
        mainParagraph
        secondParagraph
        mainImage {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
        secondImage {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
        magicianInfo {
          title
          description
          video {
            text
            url
            title
          }
        }
        activities {
          title
          mainParagraph
          secondParagraph
          characteristics {
            title
            items {
              id
              text
            }
          }
          image {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          video {
            url
            title
          }
        }
        characteristics {
          title
          items {
            id
            text
          }
          image {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
        }
        goals {
          title
          items {
            id
            text
          }
        }
        emega {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
      }
      es {
        slug
      }
    }
    educationalEventsJson {
      gl {
        contact {
          title
          description
        }
      }
    }
  }
`;

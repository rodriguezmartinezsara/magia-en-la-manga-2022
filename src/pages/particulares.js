import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { PrivateEventsContent } from '../components/pages/PrivateEventsContent';

const ParticularesEsPage = () => {
  const getData = useStaticQuery(graphql`
    query GetParticularesEsData {
      privateEventsJson {
        es {
          title
          metaDescription
          subtitle
          description
          contact {
            title
            description
          }
          typeOfEvent {
            title
            icon
            link
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            characteristics {
              id
              item
            }
          }
          servicesHeader: typeOfEvent {
            title
            icon
            link
          }
        }
        gl {
          slug
        }
      }
    }
  `);

  const privateEvents = getData.privateEventsJson.es;
  const translationSlug = getData.privateEventsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={privateEvents.title}
        description={privateEvents.metaDescription}
        lang="es"
      />
      <PrivateEventsContent privateEvents={privateEvents} />
    </Layout>
  );
};

export default ParticularesEsPage;

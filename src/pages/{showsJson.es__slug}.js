import * as React from 'react';
import Seo from '../components/Seo';
import Layout from '../components/Layout';
import { graphql } from 'gatsby';
import { ShowContent } from '../components/pages/ShowContent';

const ShowEs = props => {
  const show = props.data.showsJson.es;
  const contact = props.data.educationalEventsJson.es.contact;
  const translationSlug = props.data.showsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo title={show.title} description={show.metaDescription} lang="es" />
      <ShowContent show={show} contact={contact} />
    </Layout>
  );
};

export default ShowEs;

export const query = graphql`
  query ShowEsById($id: String) {
    showsJson(id: { eq: $id }) {
      id
      es {
        title
        metaDescription
        subtitle
        mainParagraph
        secondParagraph
        mainImage {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
        secondImage {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
        magicianInfo {
          title
          description
          video {
            text
            url
            title
          }
        }
        activities {
          title
          mainParagraph
          secondParagraph
          characteristics {
            title
            items {
              id
              text
            }
          }
          image {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          video {
            url
            title
          }
        }
        characteristics {
          title
          items {
            id
            text
          }
          image {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
        }
        goals {
          title
          items {
            id
            text
          }
        }
        emega {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
          alt
        }
      }
      gl {
        slug
      }
    }
    educationalEventsJson {
      es {
        contact {
          title
          description
        }
      }
    }
  }
`;

import * as React from 'react';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { graphql, useStaticQuery } from 'gatsby';
import { AboutUsContent } from '../components/pages/AboutUsContent';

const SobreNosotrosPage = () => {
  const getData = useStaticQuery(graphql`
    query GetSobreNosotrosData {
      aboutUsJson {
        es {
          title
          metaDescription
          sections {
            title
            subtitle
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
          }
          team {
            title
            teamMembers {
              title
              image {
                img {
                  childImageSharp {
                    gatsbyImageData(placeholder: BLURRED)
                  }
                }
                alt
              }
              description
              socialNetworks {
                icon
                link
              }
            }
          }
          contact {
            title
            description
          }
        }
        gl {
          slug
        }
      }
    }
  `);

  const aboutUs = getData.aboutUsJson.es;
  const translationSlug = getData.aboutUsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={aboutUs.title}
        description={aboutUs.metaDescription}
        lang="es"
      />
      <AboutUsContent aboutUs={aboutUs} lang="es" />
    </Layout>
  );
};

export default SobreNosotrosPage;

import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { MagicSchoolContent } from '../components/pages/MagicSchoolContent';

const EscuelaDeMagiaEsPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEscuelaData {
      magicSchoolJson {
        es {
          title
          metaDescription
          subtitle
          mainParagraph
          secondParagraph
          mainImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          secondImage {
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
            alt
          }
          signUp {
            items {
              id
              text
            }
            title
            subtitle
            link
          }
          space {
            title
            mainParagraph
            characteristics {
              items {
                id
                text
              }
            }
            video {
              url
              title
            }
          }
          faq {
            title
            questions {
              id
              question
              answer
            }
          }
          gallery {
            title
            images {
              id
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
              width
              height
            }
          }
          contact {
            title
            description
          }
        }
        gl {
          slug
        }
      }
    }
  `);

  const magicSchool = getData.magicSchoolJson.es;
  const translationSlug = getData.magicSchoolJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={magicSchool.title}
        description={magicSchool.metaDescription}
        lang="es"
      />
      <MagicSchoolContent magicSchool={magicSchool} lang={'es'} />
    </Layout>
  );
};

export default EscuelaDeMagiaEsPage;

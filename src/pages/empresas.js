import * as React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import { CorporateEventsContent } from '../components/pages/CorporateEventsContent';

const EmpresasEsPage = () => {
  const getData = useStaticQuery(graphql`
    query GetEmpresasEsData {
      corporateEventsJson {
        es {
          title
          metaDescription
          subtitle
          description
          contact {
            title
            description
          }
          typeOfEvent {
            title
            icon
            link
            description
            image {
              img {
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
              alt
            }
            characteristics {
              id
              item
            }
          }
          services: typeOfEvent {
            title
            icon
            link
          }
        }
        gl {
          slug
        }
      }
    }
  `);

  const corporateEvents = getData.corporateEventsJson.es;
  const translationSlug = getData.corporateEventsJson.gl.slug;

  return (
    <Layout lang="es" translationSlug={translationSlug}>
      <Seo
        title={corporateEvents.title}
        description={corporateEvents.metaDescription}
        lang="es"
      />
      <CorporateEventsContent corporateEvents={corporateEvents} />
    </Layout>
  );
};

export default EmpresasEsPage;

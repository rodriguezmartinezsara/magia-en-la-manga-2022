import * as React from 'react';
import Seo from '../components/Seo';
import Layout from '../components/Layout';
import { graphql } from 'gatsby';
import { TopImageBlog } from '../components/TopImageBlog';
import { BlogLayout } from '../components/BlogLayout';
import { PostLayout } from '../components/PostLayout';

const Post = props => {
  const post = props.data.postsJson;
  const content = props.data.postsJson.content;

  const allPosts = props.data.allPostsJson.nodes;
  const index = allPosts.findIndex(x => x.title === post.title);

  return (
    <Layout lang="es">
      <Seo
        title={post.title}
        description={post.metaDescription}
        lang={post.lang}
        bodyClasses="background-blog"
      />
      <TopImageBlog
        title={post.title}
        date={post.date}
        category={post.category}
      />
      <BlogLayout>
        <PostLayout
          img={post.mainImage}
          previousPost={allPosts[index - 1]}
          nextPost={allPosts[index + 1]}
          content={content}
        />
      </BlogLayout>
    </Layout>
  );
};

export default Post;

export const query = graphql`
  query PostById($id: String) {
    postsJson(id: { eq: $id }) {
      title
      metaDescription
      lang
      id
      date
      category
      mainImage {
        alt
        img {
          childImageSharp {
            gatsbyImageData(placeholder: BLURRED)
          }
        }
      }
      content {
        id
        image {
          alt
          img {
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
        }
        paragraph
        subtitle
        columns {
          image {
            alt
            img {
              childImageSharp {
                gatsbyImageData(placeholder: BLURRED)
              }
            }
          }
          paragraphs
        }
      }
    }
    allPostsJson(sort: { fields: date, order: ASC }) {
      nodes {
        title
        slug
      }
    }
  }
`;

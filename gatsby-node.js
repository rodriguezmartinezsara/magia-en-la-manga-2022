exports.createPages = async ({ actions }) => {
  const { createPage, createRedirect } = actions;
  createRedirect({
    fromPath: `/gl/inicio-galego/`,
    toPath: `/gl`,
  });
  createRedirect({
    fromPath: `/gl/particulares-galego/`,
    toPath: `/gl/particulares`,
  });
  createRedirect({
    fromPath: `/gl/eventos-publicos-galego/`,
    toPath: `/gl/eventos-publicos`,
  });
  createRedirect({
    fromPath: `/gl/empresas-galego/`,
    toPath: `/gl/empresas`,
  });
  createRedirect({
    fromPath: `/gl/the-best-magic-trick-in-the-world-galego/`,
    toPath: `/gl/the-best-magic-trick-in-the-world`,
  });
  createRedirect({
    fromPath: `/igualdad/`,
    toPath: `/espectaculo-de-magia-igualdad-de-genero`,
  });
  createRedirect({
    fromPath: `/gl/igualdade/`,
    toPath: `/gl/espectaculo-de-maxia-igualdade-de-xenero`,
  });
  createRedirect({
    fromPath: `/ingles-es/`,
    toPath: `/espectaculo-de-magia-en-ingles`,
  });
  createRedirect({
    fromPath: `/gl/ingles-gl/`,
    toPath: `/gl/espectaculo-de-maxia-en-ingles`,
  });
  createRedirect({
    fromPath: `/espectaculo-magia/`,
    toPath: `/espectaculo-de-magia`,
  });
  createRedirect({
    fromPath: `/gl/espectaculo-maxia/`,
    toPath: `/gl/espectaculo-de-maxia`,
  });
  createRedirect({
    fromPath: `/navidad/`,
    toPath: `/espectaculo-de-magia-de-navidad`,
  });
  createRedirect({
    fromPath: `/gl/nadal/`,
    toPath: `/gl/espectaculo-de-maxia-de-nadal`,
  });
  createRedirect({
    fromPath: `/halloween/`,
    toPath: `/espectaculo-de-magia-de-halloween`,
  });
  createRedirect({
    fromPath: `/gl/samain/`,
    toPath: `/gl/espectaculo-de-maxia-de-samain`,
  });
  createRedirect({
    fromPath: `/navidad-magica-en-oporto`,
    toPath: `/blog/navidad-magica-en-oporto`,
  });
  createRedirect({
    fromPath: `/que-es-la-magia-educativa-colegios-institutos`,
    toPath: `/blog/que-es-la-magia-educativa-colegios-institutos`,
  });
  createRedirect({
    fromPath: `/pont-up-store-2019`,
    toPath: `/blog/pont-up-store-2019`,
  });
};
